import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Switch } from 'react-router-dom';
import App from './pages/App';
import configureStore from './store/configureStore';
import registerServiceWorker from './registerServiceWorker';
import { AuthenticateRoute } from 'components/Routes';
import './styles/theme.css';

const store = configureStore();

render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <AuthenticateRoute path="/" component={App} />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
