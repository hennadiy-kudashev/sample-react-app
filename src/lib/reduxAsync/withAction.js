import { connect } from 'react-redux';

export default (action, stateName = Object.keys(action)[0]) =>
  connect(state => ({ ...state[stateName] }), action);
