import { isRequest, isFailure, isSuccess } from './utils';
import objectPath from 'object-path';
import cloneDeep from 'lodash/cloneDeep';

/**
 * Reducer to be used when related action is dispatched.
 *
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function reducer(state = {}, action) {
  if (isRequest(action)) {
    const newState = cloneDeep(state);

    if (action.requestTarget !== undefined) {
      objectPath.set(newState, action.requestTarget, true);
    }

    if (action.errorTarget !== undefined) {
      objectPath.del(newState, action.errorTarget);
    }

    return newState;
  } else if (isFailure(action)) {
    const newState = cloneDeep(state);

    if (action.requestTarget !== undefined) {
      objectPath.set(newState, action.requestTarget, false);
    }

    if (action.errorTarget !== undefined) {
      objectPath.set(newState, action.errorTarget, action.error);
    }

    if (action.dataTarget !== undefined) {
      objectPath.del(newState, action.dataTarget);
    }

    return newState;
  } else if (isSuccess(action)) {
    const newState = cloneDeep(state);

    if (action.requestTarget !== undefined) {
      objectPath.set(newState, action.requestTarget, false);
    }

    if (action.dataTarget !== undefined) {
      const prevData = objectPath.get(newState, action.dataTarget);
      if (prevData && action.stateAdapter) {
        objectPath.set(
          newState,
          action.dataTarget,
          action.stateAdapter(prevData, action.data)
        );
      } else {
        objectPath.set(newState, action.dataTarget, action.data);
      }
    }
    return newState;
  }
  return state;
}
