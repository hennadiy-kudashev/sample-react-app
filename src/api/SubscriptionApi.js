import ServerApi from './ServerApi';
import fakeApi from './FakeApi';
import SubscriptionType from './SubscriptionType';

class SubscriptionApi extends ServerApi {
  cancel() {
    return fakeApi.success({
      subscriptionType: SubscriptionType.TRIAL
    });
  }
  subscribe({ subscriptionType, jwt }) {
    return fakeApi.success({
      subscriptionType
    });
  }
}

export default SubscriptionApi;
