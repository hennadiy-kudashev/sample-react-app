import { updateExport } from 'actions/exportsActions';
import fakeApi from './FakeApi';
import ExportStatus from './ExportStatus';

class ExportsMessage {
  constructor(store) {
    this.store = store;
  }

  open() {
    this.websocket = new WebSocket('wss://echo.websocket.org');
    //websocket.onopen = function(evt) { onOpen(evt) };
    //websocket.onclose = function(evt) { onClose(evt) };
    this.websocket.onmessage = this._onMessage.bind(this);
    //websocket.onerror = function(evt) { onError(evt) };
  }

  startExport(item) {
    fakeApi.generate(i => {
      setTimeout(() => {
        this.websocket.send(
          JSON.stringify({
            export_id: item.export_id,
            status: i < 9 ? ExportStatus.IN_PROGRESS : ExportStatus.SUCCESS,
            progress: 10 * (i + 1),
            completed_at: i < 9 ? null : new Date().getTime(),
            csv_files:
              i < 9
                ? []
                : fakeApi.generate(
                    j => ({
                      url: `http://downloads.magimetrics.com/tasks/D90INl_aZwYn9Wpv_XxktlqwNza6hwb6Apt4X0_S/downloads/?part=${j +
                        1}`,
                      rows: 52,
                      number: j + 1
                    }),
                    5
                  )
          })
        );
      }, 1000 * (i + 1));
    });
  }

  _onMessage(payload) {
    this.store.dispatch(updateExport(JSON.parse(payload.data)));
  }
}

let exportsMessage;
export default store => {
  if (!exportsMessage) {
    exportsMessage = new ExportsMessage(store);
    exportsMessage.open();
  }
  return exportsMessage;
};
