import ServerApi from './ServerApi';
import fakeApi from './FakeApi';

class CardApi extends ServerApi {
  getCard() {
    return fakeApi.success(null);
    /*return fakeApi.success({
      name: 'John Smith',
      exp_month: 10,
      exp_year: 2020,
      last4: '4242',
      created: 1515598900 //sec
    });*/
  }

  updateCard(token) {
    const { created, card, id } = token;
    const { exp_month, exp_year, last4 } = card;
    return fakeApi.success({
      token: id,
      created,
      exp_month,
      exp_year,
      last4
    });
  }
}

export default CardApi;
