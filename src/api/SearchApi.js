import ServerApi from './ServerApi';
import SearchType from './SearchType';
import fakeApi from './FakeApi';

class SearchApi extends ServerApi {
  search({ query, search_type }) {
    if (query === 'error') {
      return fakeApi.fail({
        error_code: 'service_unavailable',
        message: 'Service is temporarily unavailable. Please try again'
      });
    }
    if (query === 'empty') {
      return fakeApi.success([]);
    }
    if (search_type === SearchType.ACCOUNT) {
      const items = fakeApi.generate(i => ({
        instagram_id: i,
        username: `user${i}`,
        name: `${query} ${search_type} ${i}`,
        thumbnail: `http://simpleqode.com/preview/sandbox/magimetrics/11/assets/img/${fakeApi.random(
          4,
          2
        )}.jpg`,
        posts: fakeApi.random(),
        followers: fakeApi.random(),
        following: fakeApi.random()
      }));
      return fakeApi.success(
        items.map(item => ({ [SearchType.ACCOUNT]: item }))
      );
    }
    if (search_type === SearchType.HASHTAG) {
      const items = fakeApi.generate(i => ({
        hashtag: `${query}_${search_type}_${i}`,
        posts: fakeApi.random()
      }));
      return fakeApi.success(
        items.map(item => ({ [SearchType.HASHTAG]: item }))
      );
    }
    if (search_type === SearchType.LOCATION) {
      const items = fakeApi.generate(i => ({
        instagram_id: i,
        location: `${query} ${search_type} ${i}`,
        thumbnail: `http://simpleqode.com/preview/sandbox/magimetrics/11/assets/img/${fakeApi.random(
          4,
          2
        )}.jpg`
      }));
      return fakeApi.success(
        items.map(item => ({ [SearchType.LOCATION]: item }))
      );
    }
  }
}

export default SearchApi;
