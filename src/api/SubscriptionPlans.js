import SubscriptionType from './SubscriptionType';

const SubscriptionPlans = [
  {
    type: SubscriptionType.BASIC,
    label: 'Basic',
    price: 39,
    features: [
      '1 export per month.',
      'Up to 100,000 followers or posts per export.'
    ]
  },
  {
    type: SubscriptionType.STANDARD,
    label: 'Standard',
    price: 99,
    features: [
      'Up to 50 exports per month.',
      'Up to 1 million followers or posts per export.',
      'Perform up to 10 exports at the same time.'
    ]
  },
  {
    type: SubscriptionType.PREMIUM,
    label: 'Premium',
    price: 299,
    features: [
      'Up to 500 exports per month.',
      'Up to 10 million followers or posts per export.',
      'Only export Instagram followers who have a certain number of followers.',
      'Perform up to 10 exports at the same time.'
    ]
  },
  {
    type: SubscriptionType.ENTERPRISE,
    label: 'Enterprise',
    askForQuote: true,
    features: [
      'Over 500 exports per month.',
      'Unlimited followers or posts per export.',
      'Only export Instagram followers who have a certain number of followers.',
      'API access.',
      'Perform 20+ exports at the same time.'
    ]
  }
];

export default SubscriptionPlans;
