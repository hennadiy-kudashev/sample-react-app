import React from 'react';
import { Link } from 'react-router-dom';
import { ExpandPanel } from 'components/Content/index';

export default [
  {
    title: 'What kind of data can I export from Instagram?',
    content: (
      <div>
        <p>
          For any <b>Instagram account</b>, you can export:
        </p>
        <ul>
          <li>accounts who follow @example</li>
          <li>accounts who @example follows</li>
          <li>posts by @example</li>
          <li>who likes @example's posts</li>
          <li>who commented on @example's posts</li>
        </ul>
        <ExpandPanel title="Show details">
          <table className="table table-responsive">
            <tbody>
              <tr>
                <th>User followers/followings</th>
                <th>User posts</th>
                <th>User likes/comments</th>
              </tr>
              <tr>
                <td>ID</td>
                <td>Date</td>
                <td>Date</td>
              </tr>
              <tr>
                <td>Username</td>
                <td>Caption</td>
                <td>Post caption</td>
              </tr>
              <tr>
                <td>Full name</td>
                <td>Used hastags</td>
                <td>Post hashtags</td>
              </tr>
              <tr>
                <td>Profile picture URL</td>
                <td>How many comments</td>
                <td>ID</td>
              </tr>
              <tr>
                <td>Bio</td>
                <td>How many likes</td>
                <td>Post type (image/video)</td>
              </tr>
              <tr>
                <td>Website</td>
                <td>Post type (image/video)</td>
                <td>Filter</td>
              </tr>
              <tr>
                <td>Number of posts</td>
                <td>Filter</td>
                <td>Link to post</td>
              </tr>
              <tr>
                <td>Number of people they follow</td>
                <td>Link</td>
                <td>Post latitude</td>
              </tr>
              <tr>
                <td>Number of followers</td>
                <td>Latitude</td>
                <td>Post longitude</td>
              </tr>
              <tr>
                <td>Date of last post</td>
                <td>Longitude</td>
                <td>Post location name</td>
              </tr>
              <tr>
                <td>Average number of likes for last 30 posts</td>
                <td>Location name</td>
                <td>Liked/commented user's instagram ID</td>
              </tr>
              <tr>
                <td>Last post latitude</td>
                <td>Image URL</td>
                <td>Username</td>
              </tr>
              <tr>
                <td>Last post longitude</td>
                <td>&nbsp;</td>
                <td>Full name</td>
              </tr>
              <tr>
                <td>Last post location name</td>
                <td>&nbsp;</td>
                <td>Profile picture URL</td>
              </tr>
              <tr>
                <td>Extracted email from bio</td>
                <td>&nbsp;</td>
                <td>Bio</td>
              </tr>
              <tr>
                <td>Profile URL</td>
                <td>&nbsp;</td>
                <td>Website</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Number of posts</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Number of people they follow</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Number of followers</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Comment date (for comments only)</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Comment ID (for comments only)</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Comment message (for comments only)</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Extracted email from bio</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Profile URL</td>
              </tr>
            </tbody>
          </table>
        </ExpandPanel>
        <p>
          For any <b>Instagram hashtag</b> you can export:
        </p>
        <ul>
          <li>the latest posts which used that</li>
          <li>who liked the posts with the hashtag</li>
          <li>who commented on the posts with the hashtag</li>
        </ul>
        <ExpandPanel title="Show details">
          <table className="table table-responsive">
            <tbody>
              <tr>
                <th>Hashtag posts</th>
                <th>Post likes/comments</th>
              </tr>
              <tr>
                <td>Date</td>
                <td>Date</td>
              </tr>
              <tr>
                <td>Caption</td>
                <td>Post caption</td>
              </tr>
              <tr>
                <td>How many comments</td>
                <td>Post type (image/video)</td>
              </tr>
              <tr>
                <td>How many likes</td>
                <td>Filter</td>
              </tr>
              <tr>
                <td>Post type (image/video)</td>
                <td>Post latitude</td>
              </tr>
              <tr>
                <td>Filter</td>
                <td>Post longitude</td>
              </tr>
              <tr>
                <td>Link</td>
                <td>Post location name</td>
              </tr>
              <tr>
                <td>Latitude</td>
                <td>Image URL</td>
              </tr>
              <tr>
                <td>Longitude</td>
                <td>Liked/commented user's instagram ID</td>
              </tr>
              <tr>
                <td>Location name</td>
                <td>Username</td>
              </tr>
              <tr>
                <td>Image URL</td>
                <td>Full name</td>
              </tr>
              <tr>
                <td>User ID</td>
                <td>Profile picture URL</td>
              </tr>
              <tr>
                <td>Username</td>
                <td>Bio</td>
              </tr>
              <tr>
                <td>Full name</td>
                <td>Website</td>
              </tr>
              <tr>
                <td>Profile picture URL</td>
                <td>Number of posts</td>
              </tr>
              <tr>
                <td>Bio</td>
                <td>Number of people they follow</td>
              </tr>
              <tr>
                <td>Website</td>
                <td>Number of followers</td>
              </tr>
              <tr>
                <td>Number of posts</td>
                <td>Date of last post</td>
              </tr>
              <tr>
                <td>Number of people they follow</td>
                <td>Average number of likes for last 30 posts</td>
              </tr>
              <tr>
                <td>Number of followers</td>
                <td>Comment date (for comments only)</td>
              </tr>
              <tr>
                <td>Date of last post</td>
                <td>Comment ID (for comments only)</td>
              </tr>
              <tr>
                <td>Average number of likes for last 30 posts</td>
                <td>Comment message (for comments only)</td>
              </tr>
              <tr>
                <td>Extracted email from bio</td>
                <td>Extracted email from bio</td>
              </tr>
              <tr>
                <td>Profile URL</td>
                <td>Profile URL</td>
              </tr>
            </tbody>
          </table>
        </ExpandPanel>
        <p>
          For any <b>location</b> you can export:
        </p>
        <ul>
          <li>the latest posts done in that location</li>
        </ul>
        <ExpandPanel title="Show details">
          <table className="table table-responsive">
            <tbody>
              <tr>
                <th>Location posts</th>
              </tr>
              <tr>
                <td>Date</td>
              </tr>
              <tr>
                <td>Caption</td>
              </tr>
              <tr>
                <td>How many comments</td>
              </tr>
              <tr>
                <td>How many likes</td>
              </tr>
              <tr>
                <td>Post type (image/video)</td>
              </tr>
              <tr>
                <td>Filter</td>
              </tr>
              <tr>
                <td>Link</td>
              </tr>
              <tr>
                <td>Latitude</td>
              </tr>
              <tr>
                <td>Longitude</td>
              </tr>
              <tr>
                <td>Location name</td>
              </tr>
              <tr>
                <td>Image URL</td>
              </tr>
              <tr>
                <td>User ID</td>
              </tr>
              <tr>
                <td>Username</td>
              </tr>
              <tr>
                <td>Full name</td>
              </tr>
              <tr>
                <td>Profile picture URL</td>
              </tr>
              <tr>
                <td>Bio</td>
              </tr>
              <tr>
                <td>Website</td>
              </tr>
              <tr>
                <td>Number of posts</td>
              </tr>
              <tr>
                <td>Number of people they follow</td>
              </tr>
              <tr>
                <td>Number of followers</td>
              </tr>
              <tr>
                <td>Date of last post</td>
              </tr>
              <tr>
                <td>Average number of likes for last 30 posts</td>
              </tr>
              <tr>
                <td>Extracted email from bio</td>
              </tr>
              <tr>
                <td>Profile URL</td>
              </tr>
            </tbody>
          </table>
        </ExpandPanel>
      </div>
    )
  },
  {
    title: "Can I export anyone's Instagram account?",
    content: (
      <p>
        Yes, as long as they have not set their account to Private, you can
        export anyone's Instagram account.
      </p>
    )
  },
  {
    title: 'How does your free trial work?',
    content: (
      <p>
        You can do 20 exports, but each export is limited to 100 rows. We
        provide the free trial so you can check how Magi Metrics works and does
        what you want, before purchasing.{' '}
        <Link to="/exports">Start free trial</Link>
      </p>
    )
  },
  {
    title:
      "Can I get Instagram user's contact information, such as email addresses?",
    content: (
      <p>
        Instagram keeps this information private for privacy reasons. However,
        some users choose to put their contact information in their Bio. Magi
        Metrics will be able to extract this and the extracted email is also one
        of the spreadsheet columns.
      </p>
    )
  },
  {
    title: 'How will the data be presented?',
    content: (
      <p>
        We will give you a CSV file, which can be opened by any spreadsheet
        program including Microsoft Excel. So you have full control over how you
        use and analyse the data.{' '}
        <a href={`${process.env.PUBLIC_URL}/sample.csv`}>
          Download a sample file
        </a>
      </p>
    )
  },
  {
    title: 'What can I use this data for?',
    content: (
      <div>
        <p>
          You can use the data to grow your business. Instagram has 600 million
          active users, which is a huge business opportunity. You can use the
          data in several ways:
        </p>
        <ul>
          <li>Find the key influencers to promote your company or campaign</li>
          <li>Get insights to your competitor</li>
          <li>Analyse your followers to help you provide better content</li>
          <li>Backup Instagram followers</li>
        </ul>
      </div>
    )
  },
  {
    title: "What's your cancellation and refund policy?",
    content: (
      <p>
        You can{' '}
        <a href="https://www.magimetrics.com/cancellation-policy.html">
          cancel
        </a>{' '}
        at any time for free. If for any reason you're not satisfied, in the
        vast majority of cases, we will give you a full refund. We like to keep
        our customers happy.
      </p>
    )
  },
  {
    title: "What's your cancellation and refund policy?",
    content: (
      <p>
        You can{' '}
        <a href="https://www.magimetrics.com/cancellation-policy.html">
          cancel
        </a>{' '}
        at any time for free. If for any reason you're not satisfied, in the
        vast majority of cases, we will give you a full refund. We like to keep
        our customers happy.
      </p>
    )
  },
  {
    title: 'Can I have a sample file?',
    content: (
      <p>
        <a href={`${process.env.PUBLIC_URL}/sample.csv`}>
          Yes. Just click here.
        </a>
      </p>
    )
  },
  {
    title: 'I got less followers/followings then user actually has. Why?',
    content: (
      <div>
        <p>There are multiple possibilities why this happened.</p>
        <ul>
          <li>
            Possibility 1: You hit your subscription plan limit. If this is the
            case you can <Link to="/upgrade">upgrade to get more data</Link>.
          </li>
          <li>
            Possibility 2: By default private accounts are not included in the
            result. But you can include them by disabling "Exclude private
            accounts from exports" before doing an export.
          </li>
        </ul>
      </div>
    )
  },
  {
    title: 'How can I transfer Instagram followers to another account?',
    content: (
      <div>
        <p>
          It's not possible to transfer Instagram followers from one account to
          another. If you're looking to get more followers then you can read
          some of our articles:
        </p>
        <p>
          <a
            target="_new"
            href="https://www.magimetrics.com/guides/how-to-increase-your-instagram-followers-by-using-hashtags.html"
          >
            Read how to increase Instagram followers by using hashtags
          </a>
          <br />
          <a
            target="_new"
            href="https://www.magimetrics.com/guides/how-to-increase-your-instagram-followers-by-following.html"
          >
            Read how to increase Instagram followers by by following
          </a>
        </p>
      </div>
    )
  },
  {
    title: 'How long can I access my exports?',
    content: (
      <p>
        We keep your exports for 3 months and you can download them at any time
        in this period. After that your exports will be deleted from our server
      </p>
    )
  }
];
