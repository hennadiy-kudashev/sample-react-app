const ExportType = {
  FOLLOWED_BY: 'followed-by',
  FOLLOWS: 'follows',
  POSTS: 'media',
  LIKES: 'likes',
  COMMENTS: 'comments'
};

export default ExportType;
