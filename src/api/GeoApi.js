import ServerApi from './ServerApi';
import fakeApi from './FakeApi';

class GeoApi extends ServerApi {
  getCountryCode() {
    return fakeApi.success('GB');
  }
}

export default GeoApi;
