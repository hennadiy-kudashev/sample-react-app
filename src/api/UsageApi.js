import ServerApi from './ServerApi';
import fakeApi from './FakeApi';

class UsageApi extends ServerApi {
  getStatistic() {
    return fakeApi.success({
      actualCount: 29,
      totalCount: 50,
      resetDate: new Date().getTime()
    });
  }
}

export default UsageApi;
