import React from 'react';
import { SampleHttp } from 'components/Format/index';
import { Json } from 'components/Format/index';

export default key => [
  {
    title: 'Overview',
    content: (
      <div>
        <div className="alert alert-info">
          While you are trialing the API, exports initiated by the API are
          limited to 100 rows each. Upgrade to the Enterprise plan to gain full
          access to the API.
        </div>
        <p>
          Requests are made by making HTTPS requests and the responses are
          returned in JSON. The response will always have a status object, which
          can take two values "ok" or "error". If there is an error then an
          error object will be returned. Depending on the request, a data object
          and a pagination object may be returned.
        </p>
        <p>
          The API is structured around three resources: accounts, hashtags and
          exports.
        </p>
      </div>
    )
  },
  {
    title: 'Authentication',
    content: (
      <div>
        <p>
          To authenticate the requests, provide the query string parameter
          'api_key'. Your API key will never expire.
        </p>
        <p>Your API key is:</p>
        <pre>
          <code>{key}</code>
        </pre>
      </div>
    )
  },
  {
    title: 'Search for an Instagram account',
    content: (
      <SampleHttp
        method="GET"
        url={`https://api.magimetrics.com/v1/accounts/search/?query=magimetrics&api_key=${key}`}
        response={{
          status: 'ok',
          data: [
            {
              username: 'magimetrics',
              profile_picture:
                'https://scontent.cdninstagram.com/t51.2885-19/s150x150/12362353_466025890253190_2091601730_a.jpg',
              id: '1627678997',
              full_name: 'Milo'
            }
          ]
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'query',
            description: 'The Instagram username you wish to search for.'
          }
        ]}
      />
    )
  },
  {
    title: "Export an Instagram account's followers",
    content: (
      <SampleHttp
        method="POST"
        url={`https://api.magimetrics.com/v1/accounts/1627678997/followed-by/export/?callback_url=http://requestb.in/15jpau21&api_key=${key}`}
        response={{
          status: 'ok',
          data: {
            export_id: 'onW8n2EinfoCp58S4bnlNZY1hmEV65vCEdjT-4pe',
            callback_url: 'http://requestb.in/15jpau21'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'filtered_search [Optional]',
            description: 'Set this to True to perform a filtered search.'
          },
          {
            key: 'min_followers [Optional]',
            description:
              'If you are performing a filtered search supply an integer.'
          },
          {
            key: 'max_followers [Optional]',
            description:
              'If you are performing a filtered search supply an integer.'
          },
          {
            key: 'min_average_likes [Optional]',
            description:
              'If you are performing a filtered search supply an integer.'
          },
          {
            key: 'max_average_likes [Optional]',
            description:
              'If you are performing a filtered search supply an integer.'
          },
          {
            key: 'callback_url [Optional]',
            description:
              'You can specify a callback URL. When the export succeeds or fails Magi Metrics will make a HTTP POST request to the specified URL with the result.'
          }
        ]}
      />
    )
  },
  {
    title: 'Export a list of the accounts an Instagram account follows',
    content: (
      <SampleHttp
        method="POST"
        url={`https://api.magimetrics.com/v1/accounts/1627678997/follows/export/?callback_url=http://requestb.in/15jpau21&api_key=${key}`}
        response={{
          status: 'ok',
          data: {
            export_id: 'onW8n2EinfoCp58S4bnlNZY1hmEV65vCEdjT-4pe',
            callback_url: 'http://requestb.in/15jpau21'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'filtered_search [Optional]',
            description: 'Set this to True to perform a filtered search.'
          },
          {
            key: 'min_followers [Optional]',
            description:
              'If you are performing a filtered search supply an integer.'
          },
          {
            key: 'max_followers [Optional]',
            description:
              'If you are performing a filtered search supply an integer.'
          },
          {
            key: 'min_average_likes [Optional]',
            description:
              'If you are performing a filtered search supply an integer.'
          },
          {
            key: 'max_average_likes [Optional]',
            description:
              'If you are performing a filtered search supply an integer.'
          },
          {
            key: 'callback_url [Optional]',
            description:
              'You can specify a callback URL. When the export succeeds or fails Magi Metrics will make a HTTP POST request to the specified URL with the result.'
          }
        ]}
      />
    )
  },
  {
    title: 'Export the posts of an Instagram account',
    content: (
      <SampleHttp
        method="POST"
        url={`https://api.magimetrics.com/v1/accounts/1627678997/posts/export/?callback_url=http://requestb.in/15jpau21&api_key=${key}`}
        response={{
          status: 'ok',
          data: {
            export_id: 'onW8n2EinfoCp58S4bnlNZY1hmEV65vCEdjT-4pe',
            callback_url: 'http://requestb.in/15jpau21'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'exclude_private_accounts [optional][default=true]',
            description: 'To exclude private accounts from exports.'
          }
        ]}
      />
    )
  },
  {
    title: 'Export who liked the posts of an Instagram account',
    content: (
      <SampleHttp
        method="POST"
        url={`https://api.magimetrics.com/v1/accounts/1627678997/likes/export/?callback_url=http://requestb.in/15jpau21&api_key=${key}`}
        response={{
          status: 'ok',
          data: {
            export_id: 'onW8n2EinfoCp58S4bnlNZY1hmEV65vCEdjT-4pe',
            callback_url: 'http://requestb.in/15jpau21'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'exclude_private_accounts [optional][default=true]',
            description: 'To exclude private accounts from exports.'
          }
        ]}
      />
    )
  },
  {
    title: 'Export who commented on the posts of an Instagram account',
    content: (
      <SampleHttp
        method="POST"
        url={`https://api.magimetrics.com/v1/accounts/1627678997/comments/export/?callback_url=http://requestb.in/15jpau21&api_key=${key}`}
        response={{
          status: 'ok',
          data: {
            export_id: 'onW8n2EinfoCp58S4bnlNZY1hmEV65vCEdjT-4pe',
            callback_url: 'http://requestb.in/15jpau21'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'exclude_private_accounts [optional][default=true]',
            description: 'To exclude private accounts from exports.'
          }
        ]}
      />
    )
  },
  {
    title: 'Search for an Instagram hashtag',
    content: (
      <SampleHttp
        method="GET"
        url={`https://api.magimetrics.com/v1/hashtags/search/?query=whitegirlinchina&api_key=${key}`}
        response={{
          status: 'ok',
          data: [
            {
              media_count: 4,
              name: 'whitegirlinchina'
            }
          ]
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'query',
            description: 'The Instagram username you wish to search for.'
          }
        ]}
      />
    )
  },
  {
    title: 'Export the posts of an Instagram hashtag',
    content: (
      <SampleHttp
        method="POST"
        url={`https://api.magimetrics.com/v1/hashtags/whitegirlinchina/posts/export/?callback_url=http://requestb.in/15jpau21&api_key=${key}`}
        response={{
          status: 'ok',
          data: {
            export_id: 'onW8n2EinfoCp58S4bnlNZY1hmEV65vCEdjT-4pe',
            callback_url: 'http://requestb.in/15jpau21'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'exclude_private_accounts [optional][default=true]',
            description: 'To exclude private accounts from exports.'
          }
        ]}
      />
    )
  },
  {
    title: 'Export who liked the posts of an Instagram hashtag',
    content: (
      <SampleHttp
        method="POST"
        url={`https://api.magimetrics.com/v1/hashtags/whitegirlinchina/likes/export/?callback_url=http://requestb.in/15jpau21&api_key=${key}`}
        response={{
          status: 'ok',
          data: {
            export_id: 'onW8n2EinfoCp58S4bnlNZY1hmEV65vCEdjT-4pe',
            callback_url: 'http://requestb.in/15jpau21'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'exclude_private_accounts [optional][default=true]',
            description: 'To exclude private accounts from exports.'
          }
        ]}
      />
    )
  },
  {
    title: 'Export who commented on the posts of an Instagram hashtag',
    content: (
      <SampleHttp
        method="POST"
        url={`https://api.magimetrics.com/v1/hashtags/whitegirlinchina/comments/export/?callback_url=http://requestb.in/15jpau21&api_key=${key}`}
        response={{
          status: 'ok',
          data: {
            export_id: 'onW8n2EinfoCp58S4bnlNZY1hmEV65vCEdjT-4pe',
            callback_url: 'http://requestb.in/15jpau21'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'exclude_private_accounts [optional][default=true]',
            description: 'To exclude private accounts from exports.'
          }
        ]}
      />
    )
  },
  {
    title: 'Search for an Instagram locations',
    content: (
      <SampleHttp
        method="GET"
        url={`https://api.magimetrics.com/v1/locations/search/?query=california&api_key=${key}`}
        response={{
          status: 'ok',
          data: [
            {
              thumbnail:
                'https://scontent.xx.fbcdn.net/v/t1.0-1/c36.36.448.448/s50x50/226405_314810885288210_316551391_n.jpg?oh=985c3e05cb7fed31efda6336f24fa8f1&oe=57C9928F',
              id: '275962599173039',
              name: 'Grupo Legal Del Sur De California TM'
            }
          ]
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          { key: '', description: 'To search locations by search query' },
          {
            key: 'query',
            description: 'The Instagram username you wish to search for.'
          },
          {
            key: '',
            description:
              'To search locations by lat/lng. Search will be made in 750m radius of the given coordinates. Export is limited to 450 places.'
          },
          {
            key: 'lat',
            description: 'Latitude of the center search coordinate.'
          },
          {
            key: 'lng',
            description: 'Longitude of the center search coordinate.'
          }
        ]}
      />
    )
  },
  {
    title: 'Export the posts of an Instagram location',
    content: (
      <SampleHttp
        method="POST"
        url={`https://api.magimetrics.com/v1/locations/42814766883/posts/export/?max_rows_to_export=50000&callback_url=http://requestb.in/15jpau21&api_key=${key}`}
        response={{
          status: 'ok',
          data: {
            export_id: 'onW8n2EinfoCp58S4bnlNZY1hmEV65vCEdjT-4pe',
            callback_url: 'http://requestb.in/15jpau21'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'max_rows_to_export [optional][default=50000]',
            description: 'To mark the amount of rows to export.'
          },
          {
            key: 'exclude_private_accounts [optional][default=true]',
            description: 'To exclude private accounts from exports.'
          }
        ]}
      />
    )
  },
  {
    title: 'View all exports',
    content: (
      <SampleHttp
        method="GET"
        url={`https://api.magimetrics.com/v1/exports/?api_key=${key}`}
        response={{
          status: 'ok',
          pagination: {
            next_cursor: null,
            more: false
          },
          data: [
            {
              status: 'Cancelled',
              callback_status_code: 200,
              description: 'Milo (magimetrics) - 52 followers',
              resource_id: '1627678997',
              started: 1463676906,
              export_id: 'GfLO4evzck8tlP_HEaCpT7vH-ThsMppg09EoJbm_',
              sub_resource_type: 'followed-by',
              failure_reason: '',
              csv_files: [
                {
                  url:
                    'https://myaccount.magimetrics.com/download-csv/from-blobstore/?blob_key=AMIfv95Ja5RLpoKUxAXgUYl4IRKVz0-nQeCxRwV8Qp5ioQRtkVkiwSRHlOlbi99VZCeCGVCQS_A0iUtwHZ_IloE2EcfgBJ9V_ocxrNMtk5BTHFjCgprLXebXM2ACct11MDmt_2en6RdTSETsCav2xYdjCf-bYix3a0E5yk3JJqtu5Ai-pua-LVotJfpX9x8VX8uwKztN1XTK',
                  rows: 52,
                  number: 1
                }
              ],
              callback_url: 'http://requestb.in/1mt3aln1',
              resource_type: 'accounts'
            }
          ]
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          },
          {
            key: 'cursor [Optional]',
            description:
              'To get the next 30 results you can supply a cursor, which is given in the response in the pagination section.'
          }
        ]}
      />
    )
  },
  {
    title: 'Delete all exports',
    content: (
      <div>
        <div className="alert alert-info">
          Warning: This will permanently delete your entire past exports
          history.
        </div>
        <SampleHttp
          method="DELETE"
          url={`https://api.magimetrics.com/v1/exports/?api_key=${key}`}
          response={{
            status: 'ok'
          }}
          parameters={[
            {
              key: 'api_key',
              description:
                'Your API key which is used to authenticate the request.'
            }
          ]}
        />
      </div>
    )
  },
  {
    title: 'View an export',
    content: (
      <SampleHttp
        method="GET"
        url={`https://api.magimetrics.com/v1/exports/GfLO4evzck8tlP_HEaCpT7vH-ThsMppg09EoJbm_/?api_key=${key}`}
        response={{
          status: 'ok',
          pagination: {
            next_cursor: null,
            more: false
          },
          data: {
            status: 'Cancelled',
            callback_status_code: 200,
            description: 'Milo (magimetrics) - 52 followers',
            resource_id: '1627678997',
            started: 1463676906,
            export_id: 'GfLO4evzck8tlP_HEaCpT7vH-ThsMppg09EoJbm_',
            sub_resource_type: 'followed-by',
            failure_reason: '',
            csv_files: [
              {
                url:
                  'https://myaccount.magimetrics.com/download-csv/from-blobstore/?blob_key=AMIfv95Ja5RLpoKUxAXgUYl4IRKVz0-nQeCxRwV8Qp5ioQRtkVkiwSRHlOlbi99VZCeCGVCQS_A0iUtwHZ_IloE2EcfgBJ9V_ocxrNMtk5BTHFjCgprLXebXM2ACct11MDmt_2en6RdTSETsCav2xYdjCf-bYix3a0E5yk3JJqtu5Ai-pua-LVotJfpX9x8VX8uwKztN1XTK',
                rows: 52,
                number: 1
              }
            ],
            callback_url: 'http://requestb.in/1mt3aln1',
            resource_type: 'accounts'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          }
        ]}
      />
    )
  },
  {
    title: 'Cancel an export',
    content: (
      <SampleHttp
        method="DELETE"
        url={`https://api.magimetrics.com/v1/exports/GfLO4evzck8tlP_HEaCpT7vH-ThsMppg09EoJbm_/?api_key=${key}`}
        response={{
          status: 'ok',
          data: {
            export_id: 'GfLO4evzck8tlP_HEaCpT7vH-ThsMppg09EoJbm_'
          }
        }}
        parameters={[
          {
            key: 'api_key',
            description:
              'Your API key which is used to authenticate the request.'
          }
        ]}
      />
    )
  },
  {
    title: 'Errors',
    content: (
      <div>
        <p>
          We've tried to make the error messages self-explanatory. Here is an
          example:
        </p>
        <Json
          data={{
            status: 'error',
            error: {
              error_type: 'api_key_missing',
              error_description:
                'The API key was not supplied in the query string.'
            }
          }}
        />
        <p>And below we've listed the possible error messages.</p>
        <table className="table table-responsive">
          <thead>
            <tr>
              <th>HTTP Status Code</th>
              <th>Error Type</th>
              <th>Error Message</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>400</td>
              <td>parameter_invalid</td>
              <td>
                The parameter 'parameter' must be supplied in the query string.
              </td>
            </tr>
            <tr>
              <td>403</td>
              <td>api_key_missing</td>
              <td>The API key was not supplied in the query string.</td>
            </tr>
            <tr>
              <td>403</td>
              <td>api_key_incorrect</td>
              <td>The API key is incorrect.</td>
            </tr>
            <tr>
              <td>404</td>
              <td>resource_not_found</td>
              <td>Resource not found.</td>
            </tr>
            <tr>
              <td>404</td>
              <td>export_not_found</td>
              <td>
                We couldn't find an export matching the specified export ID.
              </td>
            </tr>
            <tr>
              <td>405</td>
              <td>method_not_allowed</td>
              <td>The HTTP method wasn't allowed for this resource.</td>
            </tr>
            <tr>
              <td>409</td>
              <td>duplicate_export</td>
              <td>
                The export is already in progress. If you'd like to start it
                again then please cancel the previous export.
              </td>
            </tr>
            <tr>
              <td>429</td>
              <td>simultaneous_export_limit_exceeded</td>
              <td>
                You have reached the maximum number of exports which you can run
                simultaneously. This limit is in place to prevent Magi Metrics
                from becoming overloaded.
              </td>
            </tr>
            <tr>
              <td>429</td>
              <td>export_limit_exceeded</td>
              <td>You have reached the maximum number of exports per month.</td>
            </tr>
            <tr>
              <td>500</td>
              <td>internal_error</td>
              <td>There was an internal error.</td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  },
  {
    title: 'Support',
    content: (
      <p>
        If you would like help with the API, please contact us at{' '}
        <a href="mailto:support@magimetrics.com">support@magimetrics.com</a>.
      </p>
    )
  }
];
