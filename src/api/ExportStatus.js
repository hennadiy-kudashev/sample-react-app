const ExportStatus = {
  IN_QUEUE: 'In queue',
  CANCELLED: 'Cancelled',
  IN_PROGRESS: 'In progress',
  SUCCESS: 'Success',
  FAILED: 'Failed'
};

export default ExportStatus;
