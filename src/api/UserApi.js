import fakeApi from './FakeApi';
import ServerApi from './SearchApi';

class UserApi extends ServerApi {
  getAuthUser() {
    return fakeApi.success({
      name: 'user magi',
      email: 'user@marimetrics.com',
      subscriptionType: 'standard',
      apiKey: 'fake_api_key'
    });
  }

  updateUser({ firstName }) {
    return fakeApi.success({
      name: firstName
    });
  }

  changePassword({ password }) {
    return fakeApi.success();
  }

  deleteAccount() {
    return fakeApi.success();
  }
}

export default UserApi;
