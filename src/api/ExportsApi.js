import fakeApi from './FakeApi';
import ServerApi from './SearchApi';
import ExportStatus from './ExportStatus';
import { EXPORT_LIST_PAGE_SIZE } from 'const';
import getExportsMessage from './ExportsMessage';

class ExportsApi extends ServerApi {
  _item(i, isNew = false, postfix = '') {
    const status = ExportStatus[Object.keys(ExportStatus)[fakeApi.random(4)]];
    return {
      export_id: fakeApi.random().toString(),
      status: isNew ? ExportStatus.IN_QUEUE : status,
      description: `${
        isNew ? 'New' : 'Past'
      } Export ${i} (export) - 123 followers ${postfix}`,
      thumbnail: `http://simpleqode.com/preview/sandbox/magimetrics/11/assets/img/${fakeApi.random(
        4,
        2
      )}.jpg`,
      progress: isNew ? 0 : fakeApi.random(100),
      started_at: new Date().getTime() - i * 500000,
      completed_at: isNew
        ? null
        : status === ExportStatus.SUCCESS ? new Date().getTime() : null,
      time_estimation: {
        hours: 10,
        minutes: 33
      },
      csv_files:
        status !== ExportStatus.SUCCESS
          ? []
          : fakeApi.generate(
              j => ({
                url: `http://downloads.magimetrics.com/tasks/D90INl_aZwYn9Wpv_XxktlqwNza6hwb6Apt4X0_S/downloads/?part=${j +
                  1}`,
                rows: 52,
                number: j + 1
              }),
              5
            )
    };
  }

  start({ type, action, instagram_id, hashtag, filter }) {
    if (action === 'comments') {
      return fakeApi.fail({
        error_code: 'service_unavailable',
        message: 'Service is temporarily unavailable. Please try again'
      });
    }
    const result = this._item(0, true);
    if (action === 'followed-by' || action === 'follows') {
      const total = fakeApi.random(10000);
      result.followersToExport = {
        total: total,
        estimate: fakeApi.random(total)
      };
    }
    this._getItems().unshift(result);
    getExportsMessage().startExport(result);
    return fakeApi.success(result);
  }

  _getItems() {
    if (!this.items) {
      this.items = fakeApi.generate(i => this._item(i, false), 25);
    }
    return this.items;
  }

  getPasts(pageIndex, searchQuery) {
    if (searchQuery === 'error') {
      return fakeApi.fail({
        message: 'Service is temporarily unavailable. Please try again'
      });
    }
    if (searchQuery === 'empty') {
      return fakeApi.success({
        pageIndex,
        searchQuery,
        totalCount: 0,
        items: []
      });
    }
    const items = this._getItems().filter(item =>
      item.description.includes(searchQuery)
    );
    return fakeApi.success({
      pageIndex,
      searchQuery,
      totalCount: items.length,
      items: items.slice(
        pageIndex * EXPORT_LIST_PAGE_SIZE,
        (pageIndex + 1) * EXPORT_LIST_PAGE_SIZE
      )
    });
  }

  retry(id) {
    return fakeApi.success({
      export_id: id,
      status: ExportStatus.IN_QUEUE,
      progress: 0,
      started: new Date().getTime()
    });
  }

  cancel(id) {
    return fakeApi.success({
      export_id: id,
      status: ExportStatus.CANCELLED
    });
  }
}

export default ExportsApi;
