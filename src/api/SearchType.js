const SearchType = {
  ACCOUNT: 'accounts',
  HASHTAG: 'hashtags',
  LOCATION: 'locations'
};

export default SearchType;
