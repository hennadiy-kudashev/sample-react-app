const SubscriptionType = {
  TRIAL: 'trial',
  BASIC: 'basic',
  STANDARD: 'standard',
  PREMIUM: 'premium',
  ENTERPRISE: 'enterprise'
};

export default SubscriptionType;
