import React from 'react';
import { Button } from 'components/Intercom';

export default [
  {
    title: 'How does my free trial work?',
    content: <p>You can export up to 100 followers or posts per export.</p>
  },
  {
    title: 'Can I cancel any time?',
    content: (
      <p>
        You can cancel for free at any time from the 'My Plan' section. You can
        keep using Magi Metrics until the end of payment period.
      </p>
    )
  },
  {
    title: 'Can I change plans freely?',
    content: (
      <p>
        Yes! If you upgrade from one plan to another, we will only charge you
        the difference between the plans.
      </p>
    )
  },
  {
    title: 'What payment methods do you accept?',
    content: (
      <p>You can use credit cards: Visa, MasterCard or American Express.</p>
    )
  },
  {
    title: 'Can I get all likes/comments for posts?',
    content: (
      <p>
        Unfortunately no. Due to limits made by Instagram, we can only share 120
        likes and 150 comments for each post.
      </p>
    )
  },
  {
    title: 'Is there a discount for annual subscription?',
    content: (
      <p>
        Let's look at your needs together and work out a deal that suits you
        best.{' '}
        <Button className="btn btn-link">
          Contact us if you're interested!
        </Button>
      </p>
    )
  },
  {
    title: 'Is there someone I can talk about this?',
    content: (
      <p>
        <Button className="btn btn-link">Click here to contact us!</Button>
      </p>
    )
  }
];
