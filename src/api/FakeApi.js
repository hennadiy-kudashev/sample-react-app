//Api to be used during development to fake REST API call.
const delay = 1000;

const FakeApi = {
  success(result) {
    return new Promise(resolve => setTimeout(() => resolve(result), delay));
  },
  fail(error) {
    return new Promise((resolve, reject) =>
      setTimeout(() => reject(error), delay)
    );
  },
  generate(itemFn, count = 10) {
    return [...Array(count)].map((_, i) => itemFn(i));
  },
  random(max = 1000, min = 0) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
};

export default FakeApi;
