import * as types from '../actions/types';
import {
  createReducer,
  requestType,
  successType,
  failureType
} from 'lib/callAPI';
import { combineReducers } from 'redux';

const pageIndex = (state = '', action) => {
  switch (action.type) {
    case successType(types.GET_EXPORT_DATA_BY_PAGE_INDEX):
    case successType(types.GET_EXPORT_DATA_BY_SEARCH_QUERY):
      return action.response.pageIndex;
    case types.GET_EXPORT_DATA_CHANGE_PAGE_INDEX:
      return action.pageIndex;
    default:
      return state;
  }
};

const totalCount = (state = '', action) => {
  switch (action.type) {
    case failureType(types.GET_EXPORT_DATA_BY_PAGE_INDEX):
    case failureType(types.GET_EXPORT_DATA_BY_SEARCH_QUERY):
      return '';
    case successType(types.GET_EXPORT_DATA_BY_PAGE_INDEX):
    case successType(types.GET_EXPORT_DATA_BY_SEARCH_QUERY):
      return action.response.totalCount;
    case types.ADD_EXPORT_DATA:
      return state + 1;
    default:
      return state;
  }
};

const searchQuery = (state = '', action) => {
  switch (action.type) {
    case successType(types.GET_EXPORT_DATA_BY_SEARCH_QUERY):
      return action.response.searchQuery;
    default:
      return state;
  }
};

const exportItem = (state, action) => {
  switch (action.type) {
    case types.ADD_EXPORT_DATA:
      return {
        ...action.data
      };
    case types.CHANGE_EXPORT_DATA:
      if (state.export_id !== action.data.export_id) {
        return state;
      }
      return {
        ...state,
        ...action.data
      };
    default:
      return state;
  }
};

const byId = (state = {}, action) => {
  switch (action.type) {
    case failureType(types.GET_EXPORT_DATA_BY_PAGE_INDEX):
    case failureType(types.GET_EXPORT_DATA_BY_SEARCH_QUERY):
      return {};
    case successType(types.GET_EXPORT_DATA_BY_PAGE_INDEX):
    case successType(types.GET_EXPORT_DATA_BY_SEARCH_QUERY):
      const nextState = { ...state };
      action.response.items.forEach(item => {
        nextState[item.export_id] = item;
      });
      return nextState;
    /*case successType(types.GET_EXPORT_DATA_BY_SEARCH_QUERY):
      const nextState1 = {};
      action.response.items.forEach(item => {
        nextState1[item.export_id] = item;
      });
      return nextState1;*/
    case types.ADD_EXPORT_DATA:
    case types.CHANGE_EXPORT_DATA:
      return {
        ...state,
        [action.data.export_id]: exportItem(
          state[action.data.export_id],
          action
        )
      };
    default:
      return state;
  }
};

const byPage = (state = {}, action) => {
  switch (action.type) {
    case failureType(types.GET_EXPORT_DATA_BY_PAGE_INDEX):
    case failureType(types.GET_EXPORT_DATA_BY_SEARCH_QUERY):
      return {};
    case successType(types.GET_EXPORT_DATA_BY_PAGE_INDEX):
      return {
        ...state,
        [action.response.pageIndex]: action.response.items.map(
          item => item.export_id
        )
      };
    case successType(types.GET_EXPORT_DATA_BY_SEARCH_QUERY):
      return {
        [action.response.pageIndex]: action.response.items.map(
          item => item.export_id
        )
      };
    default:
      return state;
  }
};

const actionDispatching = (state = false, action) => {
  switch (action.type) {
    case requestType(types.START_EXPORT):
      return true;
    case successType(types.START_EXPORT):
      return false;
    case failureType(types.START_EXPORT):
      return false;
    default:
      return state;
  }
};

const dataReducer = combineReducers({
  pageIndex,
  totalCount,
  searchQuery,
  byId,
  byPage,
  actionDispatching
});

export default createReducer(
  [types.GET_EXPORT_DATA_BY_PAGE_INDEX, types.GET_EXPORT_DATA_BY_SEARCH_QUERY],
  dataReducer
);

export const getDataByPageIndex = state => {
  const { searchQuery, totalCount, byId, byPage, pageIndex } = state;
  const pageItems = byPage[pageIndex] || [];
  return {
    pageIndex,
    searchQuery,
    totalCount,
    items: pageItems.map(id => byId[id])
  };
};
