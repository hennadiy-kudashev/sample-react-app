import { combineReducers } from 'redux';
import initialState from '../store/initialState';
import formReducer from 'redux-form/es/reducer';
import currentUserReducer from './currentUserReducer';
import searchReducer from './searchReducer';
import pastExportsReducer from './pastExportsReducer';
import usageReducer from './usageReducer';

export default combineReducers({
  form: formReducer,
  currentUser: currentUserReducer,
  search: searchReducer,
  pastExports: pastExportsReducer,
  card: (state = initialState.card) => state,
  usage: usageReducer
});
