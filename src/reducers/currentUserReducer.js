import * as types from '../actions/types';
import initialState from '../store/initialState';

export default (state = initialState.currentUser, action) => {
  switch (action.type) {
    case types.CHANGE_CURRENT_USER:
      return {
        ...state,
        data: {
          ...state.data,
          ...action.data
        }
      };
    default:
      return state;
  }
};
