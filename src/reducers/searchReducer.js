import * as types from '../actions/types';
import initialState from '../store/initialState';

export default (state = initialState.search, action) => {
  switch (action.type) {
    case types.REMOVE_SEARCH_DATA:
      return initialState.search;
    default:
      return state;
  }
};
