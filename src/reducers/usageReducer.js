import * as types from '../actions/types';
import { createReducer, successType } from 'lib/callAPI';

const dataReducer = (state = null, action) => {
  switch (action.type) {
    case successType(types.GET_USAGE_STATISTICS):
      return action.response;
    case successType(types.START_EXPORT):
      return {
        ...state,
        actualCount: state.actualCount + 1
      };
    default:
      return state;
  }
};

export default createReducer(types.GET_USAGE_STATISTICS, dataReducer);
