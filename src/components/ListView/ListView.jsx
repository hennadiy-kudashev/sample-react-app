import React from 'react';
import PropTypes from 'prop-types';
import Pagination from '../Pagination';

class ListView extends React.Component {
  handlePageChange = ({ selected }) => {
    this.props.onPageChange(selected);
  };

  render() {
    const {
      pageData,
      pageSize,
      pageIndex,
      totalCount,
      ItemComponent,
      listClassName,
      getKeyFn
    } = this.props;
    const pageCount = Math.ceil(totalCount / pageSize);
    return (
      <div>
        <div className={listClassName}>
          {pageData.map((item, i) => (
            <ItemComponent key={getKeyFn ? getKeyFn(item) : i} item={item} />
          ))}
        </div>
        {pageCount > 1 && (
          <Pagination
            pageIndex={pageIndex}
            pageCount={pageCount}
            onPageChange={this.handlePageChange}
            countInfo={`${pageData.length} of ${totalCount}`}
          />
        )}
      </div>
    );
  }
}

ListView.propTypes = {
  pageData: PropTypes.array.isRequired,
  ItemComponent: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  onPageChange: PropTypes.func,
  getKeyFn: PropTypes.func // get unique key instead of index because when the list render next page the item component should reset it's state.
};

export default ListView;
