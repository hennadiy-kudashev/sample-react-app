import React from 'react';
import PropTypes from 'prop-types';
import ListView from './ListView';

class RemoteListView extends React.Component {
  state = {
    pageIndex: 0
  };

  handlePageChange = pageIndex => {
    if (pageIndex > this.state.pageIndex) {
      this.props.onPageDataRequest(pageIndex, this.props.pageSize);
    }
    this.setState({ pageIndex });
  };

  render() {
    const { ItemComponent, EmptyComponent, pageSize, data } = this.props;
    const { pageIndex } = this.state;

    if (!data) return null;
    if (data.items.length === 0) return <EmptyComponent />;

    const pageData = data.items.slice(
      pageIndex * pageSize,
      (pageIndex + 1) * pageSize
    );

    return (
      <ListView
        pageData={pageData}
        pageIndex={pageIndex}
        totalCount={data.totalCount}
        ItemComponent={ItemComponent}
        pageSize={pageSize}
        onPageChange={this.handlePageChange}
      />
    );
  }
}

RemoteListView.propTypes = {
  data: PropTypes.object.isRequired,
  onPageDataRequest: PropTypes.func.isRequired,
  ItemComponent: PropTypes.func.isRequired,
  EmptyComponent: PropTypes.func.isRequired,
  pageSize: PropTypes.number
};

export default RemoteListView;
