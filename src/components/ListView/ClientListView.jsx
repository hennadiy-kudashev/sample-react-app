import React from 'react';
import PropTypes from 'prop-types';
import ListView from './ListView';
import withUriState from './withUriState';

class ClientListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageIndex: props.getUriState().page - 1
    };
  }

  handlePageChange = pageIndex => {
    this.props.pushUriState({ page: pageIndex + 1 });
    //this.setState({ pageIndex });
  };

  componentWillReceiveProps(props) {
    if (this.props.isStateChanged(this.props.location, props.location)) {
      this.setState({
        pageIndex: this.props.getUriState(props.location).page - 1
      });
    }
  }

  render() {
    const {
      data,
      ItemComponent,
      pageSize,
      listClassName,
      getKeyFn
    } = this.props;
    const { pageIndex } = this.state;

    const pageData = data.slice(
      pageIndex * pageSize,
      (pageIndex + 1) * pageSize
    );
    const totalCount = data.length;
    return (
      <ListView
        getKeyFn={getKeyFn}
        pageData={pageData}
        pageIndex={pageIndex}
        totalCount={totalCount}
        ItemComponent={ItemComponent}
        pageSize={pageSize}
        onPageChange={this.handlePageChange}
        listClassName={listClassName}
      />
    );
  }
}

ClientListView.propTypes = {
  data: PropTypes.array.isRequired,
  ItemComponent: PropTypes.func.isRequired,
  pageSize: PropTypes.number
};

ListView.defaultProps = {
  pageSize: 10
};

export default withUriState(ClientListView);
