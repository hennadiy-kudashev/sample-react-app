import React from 'react';
import { stringify, parse } from 'querystring';
import { withRouter } from 'react-router-dom';
import isEqual from 'lodash/isEqual';

export default Composed => {
  return withRouter(props => {
    const extend = {
      pushUriState(state) {
        const newState = Object.assign(
          {},
          parse(props.location.search.slice(1)),
          state
        );
        props.history.push({
          pathname: props.location.pathname,
          search: stringify(newState)
        });
      },
      getUriState(location = props.location) {
        const { page = 1 } = parse(location.search.slice(1));
        return { page: parseInt(page, 10) };
      },
      isStateChanged(prevLocation, nextLocation) {
        return !isEqual(
          this.getUriState(prevLocation),
          this.getUriState(nextLocation)
        );
      }
    };

    return <Composed {...props} {...extend} />;
  });
};
