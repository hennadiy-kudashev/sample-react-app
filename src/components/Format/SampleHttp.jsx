import React from 'react';
import { Json } from 'components/Format';

const Http = ({ method, url, response, parameters }) => {
  return (
    <div>
      <p>Sample HTTPS {method} request:</p>

      <div className="row">
        <div className="col-auto">
          <pre>
            <code>{method}</code>
          </pre>
        </div>
        <div className="col">
          <pre>
            <code>{url}</code>
          </pre>
        </div>
      </div>
      <p>Sample response:</p>
      <Json data={response} />
      <p>Parameters:</p>
      <table className="table table-responsive">
        <tbody>
          {parameters.map((param, i) => (
            <tr key={i}>
              <td>{param.key}</td>
              <td>{param.description}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Http;
