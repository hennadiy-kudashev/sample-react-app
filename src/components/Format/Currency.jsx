const Currency = ({ number }) => {
  return `${number.toLocaleString(undefined, {
    maximumFractionDigits: 2,
    minimumFractionDigits: 2
  })}$`;
};

export default Currency;
