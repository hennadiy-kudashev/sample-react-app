import distanceInWords from 'date-fns/distance_in_words';

const Duration = ({ start, end, plusAgo = true }) => {
  if (end) {
    return distanceInWords(start, end, { addSuffix: plusAgo });
  }
  return distanceInWords(new Date(), start, { addSuffix: plusAgo });
};

export default Duration;
