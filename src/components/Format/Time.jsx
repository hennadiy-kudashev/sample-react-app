import React from 'react';
import PropTypes from 'prop-types';
import formatDate from 'date-fns/format';

const formats = {
  monthShort: 'MMM D, YYYY',
  monthLong: 'MMMM D, YYYY',
  dateTime: 'MMMM D, YYYY, hh:mm a',
  date: 'DD/MM/YYYY'
};

const Time = ({ ms, format }) => {
  const date = new Date(ms);
  return (
    <time dateTime={date.toISOString()}>
      {formatDate(date, formats[format])}
    </time>
  );
};

Time.propTypes = {
  ms: PropTypes.number.isRequired,
  format: PropTypes.oneOf(Object.keys(formats)).isRequired
};

export default Time;
