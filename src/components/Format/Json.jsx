import React from 'react';
import hljs from 'highlight.js/lib/highlight';
import json from 'highlight.js/lib/languages/json';
import 'highlight.js/styles/atom-one-light.css';
import { findDOMNode } from 'react-dom';

class Json extends React.Component {
  componentDidMount() {
    hljs.registerLanguage('json', json);
    hljs.highlightBlock(findDOMNode(this.refs.code));
  }

  render() {
    const { data } = this.props;
    return (
      <pre className="pre-scrollable">
        <code ref="code">{JSON.stringify(data, null, 2)}</code>
      </pre>
    );
  }
}

export default Json;
