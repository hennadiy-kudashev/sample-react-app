import React from 'react';

const ContainerWithRightBlock = ({ rightBlock: RightBlock, children }) => {
  return (
    <div className="container">
      <div className="row align-items-start">
        <div className="col-lg-4 order-lg-2">
          <RightBlock />
        </div>
        <div className="col-lg-8 order-lg-1">{children}</div>
      </div>
    </div>
  );
};

export default ContainerWithRightBlock;
