import React from 'react';
import { AsideMenu } from 'components/Menu';

const ContainerWithAside = ({ menuItems, children }) => (
  <div className="container-fluid">
    <div className="row">
      <div className="col-12 col-md-4 col-lg-3 col-xl-2">
        <AsideMenu items={menuItems} />
      </div>
      <div className="col-12 col-md-8 col-lg-9 col-xl-10">{children}</div>
    </div>
  </div>
);

export default ContainerWithAside;
