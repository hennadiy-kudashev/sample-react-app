import React from 'react';
import cx from 'classnames';
import './Section.css';

const Section = ({ className, children }) => (
  <section className={cx('section', className)}>{children}</section>
);

Section.Heading = ({ children }) => (
  <h4 className="section-heading">{children}</h4>
);

Section.Subheading = ({ children }) => (
  <p className="section-subheading">{children}</p>
);

export default Section;
