import React from 'react';
import Collapse from 'reactstrap/lib/Collapse';

class ExpandPanel extends React.Component {
  state = {
    collapsed: true
  };

  handleToggleCollapse = () => {
    this.setState(({ collapsed }) => ({ collapsed: !collapsed }));
  };

  render() {
    const { title, children } = this.props;
    const { collapsed } = this.state;
    return (
      <div>
        <button className="btn btn-link" onClick={this.handleToggleCollapse}>
          {title}
        </button>
        <Collapse isOpen={!collapsed}>{children}</Collapse>
      </div>
    );
  }
}

export default ExpandPanel;
