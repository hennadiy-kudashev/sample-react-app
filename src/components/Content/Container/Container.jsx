import React from 'react';

const Container = ({ children, title }) => (
  <div className="container">
    {title && (
      <div className="row">
        <div className="col">
          <h3 className="section-heading text-center">{title}</h3>
        </div>
      </div>
    )}
    <div className="row">
      <div className="col">{children}</div>
    </div>
  </div>
);

export default Container;
