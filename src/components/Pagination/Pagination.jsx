import React from 'react';
import ReactPaginate from 'react-paginate';
import './Pagination.css';

const Pagination = ({ pageCount, pageIndex, onPageChange, countInfo }) => (
  <div className="pagination-container">
    <div className="row align-items-center">
      <div className="col-md">
        <ReactPaginate
          previousLabel={<i className="ion-ios-arrow-left" />}
          nextLabel={<i className="ion-ios-arrow-right" />}
          breakLabel="..."
          breakClassName="page-link"
          forcePage={pageIndex}
          pageCount={pageCount}
          marginPagesDisplayed={3}
          pageRangeDisplayed={3}
          onPageChange={onPageChange}
          containerClassName="pagination"
          pageClassName="page-item"
          previousClassName="page-item"
          nextClassName="page-item"
          pageLinkClassName="page-link"
          previousLinkClassName="page-link"
          nextLinkClassName="page-link"
          activeClassName="active"
          disabledClassName="disabled"
        />
      </div>
      <div className="col-md">
        <p>Showing {countInfo}</p>
      </div>
    </div>
  </div>
);

export default Pagination;
