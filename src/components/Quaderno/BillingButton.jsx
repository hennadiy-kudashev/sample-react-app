import React from 'react';
import { Button } from 'components/Form';
import { QUADERNO_PUBLISHABLE_KEY } from 'const';
import ScriptLoader from 'components/ScriptLoader';

const BillingButton = ({ customerID }) => {
  const handleScriptLoad = () => {
    this.handler = window.QuadernoBilling.configure({
      key: QUADERNO_PUBLISHABLE_KEY,
      customer_id: customerID,
      editable: true
    });
  };

  const handleClick = () => {
    this.handler.open();
  };

  return (
    <ScriptLoader
      url="https://billing.quaderno.io/billing.js"
      onLoad={handleScriptLoad}
    >
      <Button type="button" className="btn btn-primary" onClick={handleClick}>
        View invoices
      </Button>
    </ScriptLoader>
  );
};

export default BillingButton;
