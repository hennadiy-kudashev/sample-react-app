import React from 'react';
import SubmissionError from 'redux-form/es/SubmissionError';

export default Composed => {
  return props => {
    const createSubscription = () => {
      return new Promise((resolve, reject) =>
        window.Quaderno.createSubscription({
          success: (status, response) => resolve(response.details),
          error: (status, response) => reject(response)
        })
      ).catch(error => {
        throw new SubmissionError({ _error: error.message });
      });
    };
    return <Composed {...props} createSubscription={createSubscription} />;
  };
};
