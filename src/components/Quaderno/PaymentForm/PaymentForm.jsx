import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './PaymentForm.css';
import reduxForm from 'redux-form/es/reduxForm';
import Field from 'redux-form/es/Field';
import SubmissionError from 'redux-form/es/SubmissionError';
import actions from 'redux-form/es/actions';
import { Button, CountrySelect, FormGroupField } from 'components/Form';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Card as CardForm, withCreateToken } from 'components/Stripe';
import withCreateSubscription from '../withCreateSubscription';
import { QUADERNO_PUBLISHABLE_KEY } from 'const';
import cx from 'classnames';
import Alert from 'components/Alert';
import ScriptLoader from 'components/ScriptLoader';
import Card from 'pages/Invoices/Cards/Card';
import GeoApi from 'api/GeoApi';

class PaymentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vat: 0,
      cardToken: props.card ? props.card.token : ''
    };
  }

  handleSubmit = values => {
    if (this.state.cardToken) {
      return this.createSubscription();
    } else {
      return this.props.createToken(values).then(token => {
        this.setState({ cardToken: token.id });
        return this.createSubscription();
      });
    }
  };
  createSubscription = () => {
    return this.props.createSubscription().then(jwt =>
      this.props
        .onSubscription({ jwt, subscriptionType: this.props.plan.type })
        .catch(error => {
          throw new SubmissionError({ _error: error.message });
        })
    );
  };

  handleScriptLoad = () => {
    //window.Quaderno.init('#payment-form');
    document
      .getElementById('payment-form')
      .addEventListener('taxCalculated.Quaderno', data => {
        this.setState({ vat: data.detail.tax.rate });
      });
  };

  componentDidMount() {
    new GeoApi().getCountryCode().then(code => {
      this.props.change('country', code);
      window.Quaderno.init('#payment-form');
    });
  }

  render() {
    const {
      plan,
      card,
      handleSubmit,
      submitting,
      pristine,
      error
    } = this.props;
    const { vat, cardToken } = this.state;
    const { type, label, price } = plan;

    return (
      <ScriptLoader
        url="https://js.quaderno.io/v2/"
        onLoad={this.handleScriptLoad}
      >
        <form
          className="billing-form"
          id="payment-form"
          onSubmit={handleSubmit(this.handleSubmit)}
          data-key={QUADERNO_PUBLISHABLE_KEY}
          data-plan={type}
          data-amount={price * 100}
          data-taxes="excluded"
        >
          <div className="billing-form-info">
            <div className="row">
              <div className="col">
                <h5 className="billing-heading">Personal info:</h5>
              </div>
            </div>
            <div className="row">
              <div className="col-md">
                <Field
                  name="email"
                  data-quaderno="email"
                  component="input"
                  type="hidden"
                />
                <input
                  type="hidden"
                  data-quaderno="cardToken"
                  value={cardToken}
                />
                <FormGroupField
                  name="firstName"
                  label="First name"
                  data-quaderno="first-name"
                  required
                />
                <FormGroupField
                  name="lastName"
                  data-quaderno="last-name"
                  label="Last name"
                />
              </div>
            </div>
            <div className="row">
              <div className="col-md">
                <FormGroupField
                  name="country"
                  label="Country"
                  data-quaderno="country"
                  component={CountrySelect}
                  required
                />
                <FormGroupField
                  name="companyName"
                  label="Company name"
                  data-quaderno="company-name"
                />
                <div className={vat > 0 ? '' : 'd-none'}>
                  <FormGroupField
                    name="vatNumber"
                    label="VAT Number"
                    data-quaderno="vat-number"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="billing-form-card">
            {card ? <Card data={card} /> : <CardForm />}
          </div>
          <div className="billing-form-pre">
            <div className="row">
              <div className="col">
                <h6 className="billing-form-heading">{label} plan:</h6>
              </div>
              <div className="col-auto">
                <div className="billing-form-price">
                  $<span className="quaderno-subtotal" />
                </div>
              </div>
            </div>
          </div>
          <div className={cx('billing-form-pre', vat > 0 ? '' : 'd-none')}>
            <div className="row">
              <div className="col">
                <h6 className="billing-form-heading">VAT {vat}%:</h6>
              </div>
              <div className="col-auto">
                <div className="billing-form-price">
                  $<span className="quaderno-taxes" />
                </div>
              </div>
            </div>
          </div>
          <div className="billing-form-total">
            <div className="row align-items-center">
              <div className="col">
                <h6 className="billing-form-heading">
                  <strong>Total:</strong>
                </h6>
              </div>
              <div className="col-auto">
                <div className="billing-form-price">
                  <strong>
                    $<span className="quaderno-total" />
                  </strong>
                </div>
              </div>
            </div>
          </div>
          {error && <Alert type="error">{error}</Alert>}
          <div className="billing-form-submit">
            <Button
              type="submit"
              className="btn btn-primary"
              disabled={pristine}
              loading={submitting}
            >
              Subscribe
            </Button>
            <Link to="/my-plan" className="btn btn-light">
              Cancel
            </Link>
          </div>
        </form>
      </ScriptLoader>
    );
  }
}

PaymentForm.propTypes = {
  plan: PropTypes.object.isRequired,
  initialValues: PropTypes.object,
  onSubscription: PropTypes.func.isRequired
};

export default compose(
  connect(undefined, { change: actions.change }),
  withCreateToken,
  withCreateSubscription,
  reduxForm({
    form: 'billing'
  })
)(PaymentForm);
