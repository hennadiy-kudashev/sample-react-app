import React from 'react';
import Select from './Select';
import CountryList from 'country-list';

const CountrySelect = props => {
  return (
    <Select
      {...props}
      options={CountryList()
        .getData()
        .map(country => ({
          label: country.name,
          value: country.code
        }))}
    />
  );
};

export default CountrySelect;
