import React from 'react';
import TextInput from './TextInput';

const FormGroupInput = ({
  render: Render,
  meta,
  label,
  labelAddon: LabelAddon,
  required,
  input,
  ...rest
}) => {
  const { name } = input;
  const { error, touched } = meta;
  return (
    <div className="form-group">
      <label htmlFor={name}>
        {label}
        {required && ' *'}
        <LabelAddon />
      </label>
      <Render input={input} meta={meta} {...rest} />
      {error && touched && <div className="invalid-feedback">{error}</div>}
    </div>
  );
};

FormGroupInput.defaultProps = {
  render: TextInput,
  labelAddon: () => null
};

export default FormGroupInput;
