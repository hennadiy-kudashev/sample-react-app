import React from 'react';
import cx from 'classnames';

const TextInput = ({ meta: { error, touched }, input, ...rest }) => {
  return (
    <input
      type="text"
      {...input}
      {...rest}
      className={cx('form-control', error && touched ? 'is-invalid' : '')}
    />
  );
};

export default TextInput;
