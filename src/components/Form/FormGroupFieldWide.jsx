import React from 'react';
import Field from 'redux-form/es/Field';
import FormGroupInputWide from './FormGroupInputWide';

const requiredValidator = value => (value ? undefined : 'Required');

const FormGroupFieldWide = ({ component: Component, ...props }) => {
  const newProps = { ...props };
  if (props.required) {
    newProps.validate = requiredValidator;
  }
  return (
    <Field {...newProps} component={FormGroupInputWide} render={Component} />
  );
};

export default FormGroupFieldWide;
