import React from 'react';
import cx from 'classnames';

const Select = ({ meta: { error, touched }, input, options = [], ...rest }) => {
  return (
    <select
      {...input}
      {...rest}
      className={cx('custom-select', error && touched ? 'is-invalid' : '')}
    >
      <option />
      {options.map(option => (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      ))}
    </select>
  );
};

export default Select;
