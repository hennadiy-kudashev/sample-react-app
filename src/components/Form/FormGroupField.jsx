import React from 'react';
import Field from 'redux-form/es/Field';
import FormGroupInput from './FormGroupInput';

const requiredValidator = value => (value ? undefined : 'Required');

const FormGroupField = ({ component: Component, ...props }) => {
  const newProps = { ...props };
  if (props.required) {
    newProps.validate = requiredValidator;
  }
  return <Field {...newProps} component={FormGroupInput} render={Component} />;
};

export default FormGroupField;
