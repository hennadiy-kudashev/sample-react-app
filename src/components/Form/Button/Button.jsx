import React from 'react';
import { Spin } from 'components/Loader';
import cx from 'classnames';
import './Button.css';

const Button = ({ children, className, loading = false, ...props }) => {
  const loadingClassName = {
    'btn-loading': true,
    'd-none': !loading
  };
  const contextClasses = { 'btn-content-loading': loading };
  return (
    <button
      {...props}
      className={cx('app-btn', className)}
      disabled={props.disabled || loading}
    >
      <Spin className={cx(loadingClassName)} />
      <span className={cx(contextClasses)}>{children}</span>
    </button>
  );
};

export default Button;
