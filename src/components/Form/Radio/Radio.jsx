import React from 'react';

const Radio = ({ name, label, type, children, checked, onChange }) => {
  return (
    <label className={`custom-control custom-${type}`}>
      <input
        type={type}
        className="custom-control-input"
        name={name}
        checked={checked}
        onChange={onChange}
      />
      <span className="custom-control-indicator" />
      <span className="custom-control-description">{label || children}</span>
    </label>
  );
};

Radio.defaultProps = {
  type: 'radio',
  name: ''
};

export default Radio;
