import React from 'react';
import TextInput from './TextInput';

const FormGroupInput = ({
  render: Render,
  meta,
  label,
  required,
  input,
  ...rest
}) => {
  const { name } = input;
  const { error, touched } = meta;
  return (
    <div className="form-group">
      <div className="row align-items-center">
        <div className="col-md-3 col-lg-2">
          <label htmlFor={name}>
            {label}
            {required && ' *'}
          </label>
        </div>
        <div className="col-md-9 col-lg-10">
          <Render input={input} meta={meta} {...rest} />
        </div>
        {error &&
          touched && (
            <div className="col-12 col-md-9 col-lg-10  offset-md-3 offset-lg-2">
              <div className="invalid-feedback">{error}</div>
            </div>
          )}
      </div>
    </div>
  );
};

FormGroupInput.defaultProps = {
  render: TextInput
};

export default FormGroupInput;
