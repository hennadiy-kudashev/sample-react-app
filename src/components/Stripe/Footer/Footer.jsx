import React from 'react';
import poweredByStripe from 'images/powered_by_stripe.svg';
import './Footer.css';

const Footer = () => {
  return (
    <section className="stripe-footer">
      <p className="text-muted">
        Secure payment <img src={poweredByStripe} alt="Powered by Stripe" />
      </p>
    </section>
  );
};

export default Footer;
