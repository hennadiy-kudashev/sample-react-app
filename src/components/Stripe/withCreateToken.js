import React from 'react';
import SubmissionError from 'redux-form/es/SubmissionError';

export default Composed => {
  return props => {
    const createToken = values => {
      return new Promise(resolve =>
        window.Stripe.createToken(values.card, (_, response) =>
          resolve(response)
        )
      ).then(response => {
        if (response.error) {
          if (response.error.param) {
            throw new SubmissionError({
              card: { [response.error.param]: response.error.message }
            });
          }
          throw new SubmissionError({ _error: response.error.message });
        }
        return response;
      });
    };
    return <Composed {...props} createToken={createToken} />;
  };
};
