import React from 'react';
import { Button } from 'components/Form';
import { Link } from 'react-router-dom';
import Alert from 'components/Alert';
import reduxForm from 'redux-form/es/reduxForm';
import SubmissionError from 'redux-form/es/SubmissionError';
import './CardForm.css';
import Card from './Card';
import { compose } from 'redux';
import withCreateToken from '../withCreateToken';

class CardForm extends React.Component {
  handleSubmit = values => {
    return this.props.createToken(values).then(token => {
      return this.props.onTokenReceived(token).catch(error => {
        throw new SubmissionError({ _error: error.message });
      });
    });
  };

  render() {
    const { handleSubmit, submitting, error, pristine } = this.props;
    return (
      <form
        className="cards-form mt-4"
        onSubmit={handleSubmit(this.handleSubmit)}
      >
        {error && <Alert type="error">{error}</Alert>}
        <Card />
        <div className="row">
          <div className="col cards-form-submit">
            <Button
              type="submit"
              className="btn btn-primary"
              disabled={pristine}
              loading={submitting}
            >
              Update
            </Button>
            <Link to="/invoices-and-billing" className="btn btn-light">
              Cancel
            </Link>
          </div>
        </div>
      </form>
    );
  }
}

export default compose(
  withCreateToken,
  reduxForm({
    form: 'card'
  })
)(CardForm);
