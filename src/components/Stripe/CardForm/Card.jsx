import React from 'react';
import mastercard from 'images/mastercard.svg';
import visa from 'images/visa.svg';
import americanexpress from 'images/american-express.svg';
import ScriptLoader from 'components/ScriptLoader';
import { FormGroupField } from 'components/Form';
import FormSection from 'redux-form/es/FormSection';
import { STRIPE_PUBLISHABLE_KEY } from 'const';

const Card = () => {
  const handleScriptLoad = () => {
    window.Stripe.setPublishableKey(STRIPE_PUBLISHABLE_KEY);
  };
  return (
    <ScriptLoader url="https://js.stripe.com/v2/" onLoad={handleScriptLoad}>
      <FormSection name="card">
        <div className="row">
          <div className="col">
            <h5 className="cards-form-heading">Card details:</h5>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <FormGroupField
              name="number"
              label="Card number"
              labelAddon={() => (
                <span className="label-img">
                  <img src={visa} alt="..." />
                  <img src={mastercard} alt="..." />
                  <img src={americanexpress} alt="..." />
                </span>
              )}
              required
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md">
            <FormGroupField
              name="exp_month"
              label="Exp. month"
              type="number"
              placeholder="MM"
              required
            />
          </div>
          <div className="col-md">
            <FormGroupField
              name="exp_year"
              label="Exp. year"
              type="number"
              placeholder="YYYY"
              required
            />
          </div>
          <div className="col-md">
            <FormGroupField
              name="cvc"
              label="CVC"
              type="number"
              placeholder="e.g. 331"
              required
            />
          </div>
        </div>
      </FormSection>
    </ScriptLoader>
  );
};

export default Card;
