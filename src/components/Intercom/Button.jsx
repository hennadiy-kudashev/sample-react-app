import React from 'react';

const Button = ({ children, ...props }) => {
  const handleClick = () => {
    window.Intercom('showNewMessage', '');
  };
  return (
    <button {...props} onClick={handleClick}>
      {children}
    </button>
  );
};

export default Button;
