import React from 'react';
import { withCurrentUser } from 'components/Connectors';
import Intercom from 'react-intercom';
import { INTERCOM_APP_ID } from 'const';

const Chat = ({ currentUser }) => {
  const user = {
    email: currentUser.email,
    name: currentUser.name
  };
  return <Intercom appID={INTERCOM_APP_ID} {...user} />;
};

export default withCurrentUser(Chat);
