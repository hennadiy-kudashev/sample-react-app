import React from 'react';
import PropTypes from 'prop-types';
import { getAuthUser } from 'actions/userActions';
import { withAction } from 'lib/reduxAsync';
import { Loading } from 'components/Content';

class AuthenticateRoute extends React.Component {
  componentWillMount() {
    this.props.getAuthUser();
  }

  render() {
    const { component: C, loading, data, error, ...props } = this.props;

    if (loading) {
      return <Loading fullScreen />;
    }

    if (error) {
      window.location.href = `https://myaccount.magimetrics.com/login/?redirect=${
        window.location.pathname
      }`;
    }
    if (data) {
      return <C {...props} />;
    }
    return null;
  }
}

AuthenticateRoute.propTypes = {
  component: PropTypes.func.isRequired
};

export default withAction({ getAuthUser }, 'currentUser')(AuthenticateRoute);
