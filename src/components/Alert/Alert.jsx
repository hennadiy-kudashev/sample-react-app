import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './Alert.css';

const Alert = ({ type, children }) => {
  const className = {
    alert: true,
    'alert-success': type === 'success',
    'alert-info': type === 'info',
    'alert-danger': type === 'error'
  };
  const iconClassName = {
    'ion-checkmark-circled': type === 'success',
    'ion-information-circled': type === 'info',
    'ion-close-circled': type === 'error'
  };
  return (
    <div className="row">
      <div className="col">
        <div className={cx(className)}>
          <i className={cx(iconClassName)} />
          {children}
        </div>
      </div>
    </div>
  );
};

Alert.propTypes = {
  type: PropTypes.oneOf(['info', 'error', 'success']).isRequired,
  children: PropTypes.node
};

export default Alert;
