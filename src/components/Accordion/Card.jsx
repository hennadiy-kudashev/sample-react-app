import React from 'react';
import Collapse from 'reactstrap/lib/Collapse';
import PropTypes from 'prop-types';
import uniqueId from 'lodash/uniqueId';
import './Card.css';

class Card extends React.Component {
  state = {
    collapsed: false
  };

  toggle = () => {
    this.setState(({ collapsed }) => ({ collapsed: !collapsed }));
  };

  componentWillMount() {
    this.id = uniqueId('card_item_');
  }

  render() {
    const { title, children } = this.props;
    const { collapsed } = this.state;

    return (
      <div className="card">
        <div className="card-header" role="tab" id={`${this.id}_heading`}>
          <h5>
            <span
              onClick={this.toggle}
              aria-expanded={collapsed}
              aria-controls={`${this.id}_collapse`}
            >
              {title}
            </span>
          </h5>
        </div>
        <Collapse
          isOpen={collapsed}
          role="tabpanel"
          aria-labelledby={`${this.id}_heading`}
          id={`${this.id}_collapse`}
        >
          <div className="card-body">{children}</div>
        </Collapse>
      </div>
    );
  }
}

Card.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node
};

export default Card;
