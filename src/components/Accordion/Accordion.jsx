import React from 'react';

const Accordion = ({ children }) => {
  return (
    <div className="faq-accordion" role="tablist">
      {children}
    </div>
  );
};

export default Accordion;
