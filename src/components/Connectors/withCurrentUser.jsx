import { connect } from 'react-redux';
import SubscriptionType from 'api/SubscriptionType';

const mapStateToProps = ({ currentUser }) => ({
  currentUser: {
    ...currentUser.data,
    isTrial: currentUser.data.subscriptionType === SubscriptionType.TRIAL
  }
});

export default connect(mapStateToProps);
