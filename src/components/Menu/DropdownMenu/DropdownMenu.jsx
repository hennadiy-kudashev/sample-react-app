import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { UncontrolledDropdown } from 'reactstrap/lib/Uncontrolled';
import DropdownToggle from 'reactstrap/lib/DropdownToggle';
import DropdownMenu from 'reactstrap/lib/DropdownMenu';
import DropdownItem from 'reactstrap/lib/DropdownItem';
import cx from 'classnames';

const MenuDropdown = ({ head: Head, items, location }) => {
  const activeClass = {
    active: items.some(item => location.pathname.startsWith(item.url))
  };
  return (
    <UncontrolledDropdown nav>
      <DropdownToggle nav caret className={cx(activeClass)}>
        <Head />{' '}
      </DropdownToggle>
      <DropdownMenu>
        {items.map(item => {
          const props = {
            key: item.label
          };
          if (item.tag) {
            Object.assign(props, {
              tag: item.tag,
              href: item.url
            });
          } else {
            Object.assign(props, {
              tag: NavLink,
              to: item.url
            });
          }
          return <DropdownItem {...props}>{item.label}</DropdownItem>;
        })}
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

export default withRouter(MenuDropdown);
