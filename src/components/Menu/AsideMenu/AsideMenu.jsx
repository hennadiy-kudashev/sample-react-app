import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-scroll/modules/components/Link';
import './AsideMenu.css';

const AsideMenu = ({ items }) => (
  <div className="nav nav-aside">
    {items.map(item => (
      <Link
        key={item.url}
        to={item.url}
        href={`#${item.url}`}
        className="nav-link"
        smooth={true}
        spy={true}
        offset={-25}
        duration={700}
      >
        {item.label}
      </Link>
    ))}
  </div>
);

AsideMenu.propTypes = {
  items: PropTypes.array.isRequired
};

export default AsideMenu;
