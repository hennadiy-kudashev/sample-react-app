import React from 'react';
import LoadScript from 'react-load-script';
import { Loading } from 'components/Content';
import Alert from 'components/Alert';

class ScriptLoader extends React.Component {
  state = {
    scriptLoading: true,
    error: ''
  };

  handleOnLoad = () => {
    this.setState(
      {
        scriptLoading: false
      },
      () => this.props.onLoad()
    );
  };

  handleOnError = () => {
    this.setState({
      error: `Fail to load external script (${this.props.url}).`
    });
  };

  renderContent() {
    const { scriptLoading, error } = this.state;
    const { children } = this.props;
    if (error) {
      return <Alert type="error">{error}</Alert>;
    }
    if (scriptLoading) {
      return <Loading />;
    }
    return children;
  }

  render() {
    const { url } = this.props;

    return (
      <div>
        <LoadScript
          url={url}
          onLoad={this.handleOnLoad}
          onError={this.handleOnError}
        />
        {this.renderContent()}
      </div>
    );
  }
}

export default ScriptLoader;
