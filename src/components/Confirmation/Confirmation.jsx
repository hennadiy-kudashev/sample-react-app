import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Section } from 'components/Content';
import './Confirmation.css';

const Confirmation = ({
  type,
  header,
  subheader,
  buttonText,
  buttonProps,
  children
}) => {
  const iconClassName = {
    'ion-ios-checkmark-outline': type === 'info',
    'ion-ios-close-outline': type === 'error'
  };
  return (
    <Section className={cx('confirmation', type)}>
      <h4 className="confirmation-heading">
        <i className={cx(iconClassName)} /> {header}
      </h4>
      {subheader && <p className="confirmation-subheading">{subheader}</p>}
      {children}
      {buttonText && (
        <div className="confirmation-btn">
          <button className="btn btn-primary text-lowercase" {...buttonProps}>
            {buttonText}
          </button>
        </div>
      )}
    </Section>
  );
};

Confirmation.Content = ({ children }) => (
  <p className="confirmation-content">{children}</p>
);

Confirmation.propTypes = {
  type: PropTypes.oneOf(['info', 'error']).isRequired,
  header: PropTypes.string.isRequired,
  subheader: PropTypes.node,
  children: PropTypes.node,
  buttonText: PropTypes.string,
  buttonProps: PropTypes.object
};

export default Confirmation;
