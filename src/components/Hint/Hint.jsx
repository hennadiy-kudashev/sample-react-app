import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'reactstrap/lib/Popover';
import PopoverBody from 'reactstrap/lib/PopoverBody';
import uniqueId from 'lodash/uniqueId';

class Hint extends React.Component {
  state = {
    popoverOpen: false
  };

  handleToggle = () => {
    this.setState(({ popoverOpen }) => ({ popoverOpen: !popoverOpen }));
  };

  componentWillMount() {
    this.id = uniqueId('popover_');
  }

  render() {
    return [
      <i
        key="icon"
        className="ion-information-circled"
        id={this.id}
        onMouseEnter={this.handleToggle}
        onMouseLeave={this.handleToggle}
      />,
      <Popover
        key="popover"
        placement="right"
        target={this.id}
        isOpen={this.state.popoverOpen}
      >
        <PopoverBody>{this.props.children}</PopoverBody>
      </Popover>
    ];
  }
}

Hint.propTypes = {
  children: PropTypes.node.isRequired
};

export default Hint;
