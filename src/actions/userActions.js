import UserApi from '../api/UserApi';
import { fromTo } from 'lib/reduxAsync';
import * as types from './types';
import actions from 'redux-form/es/actions';

export const getAuthUser = () => {
  return fromTo(() => new UserApi().getAuthUser(), ['currentUser']);
};

export const updateUser = user => dispatch => {
  return new UserApi().updateUser(user).then(data => {
    dispatch(updateCurrentUser(data));
  });
};

export const changePassword = params => dispatch => {
  return new UserApi().changePassword(params).then(() => {
    dispatch(actions.reset('changePassword'));
  });
};

export const updateCurrentUser = data => {
  return { type: types.CHANGE_CURRENT_USER, data };
};

export const deleteAccount = () => dispatch => {
  return new UserApi().deleteAccount().then(() => {});
};
