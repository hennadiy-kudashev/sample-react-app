import SubscriptionApi from 'api/SubscriptionApi';
import { updateCurrentUser } from './userActions';

export const cancelSubscription = () => dispatch => {
  return new SubscriptionApi().cancel().then(data => {
    dispatch(updateCurrentUser(data));
  });
};

export const subscribe = params => dispatch => {
  return new SubscriptionApi().subscribe(params).then(data => {
    dispatch(updateCurrentUser(data));
  });
};
