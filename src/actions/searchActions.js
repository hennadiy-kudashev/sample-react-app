import SearchApi from 'api/SearchApi';
import { fromTo } from 'lib/reduxAsync';
import * as types from './types';

export const search = criteria => {
  return fromTo(() => new SearchApi().search(criteria), ['search']);
};

export const removeSearchData = () => {
  return { type: types.REMOVE_SEARCH_DATA };
};
