import UsageApi from '../api/UsageApi';
import * as types from './types';
import { createAction } from 'lib/callAPI';

export const getUsageStatistics = () => {
  return createAction({
    type: types.GET_USAGE_STATISTICS,
    callAPI: () => new UsageApi().getStatistic(),
    shouldCallAPI: state => !state.usage.data
  });
};
