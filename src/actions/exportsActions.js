import ExportsApi from '../api/ExportsApi';
import * as types from './types';
import { createAction } from 'lib/callAPI';

const exportApi = new ExportsApi();

export const getPastExportsByPageIndex = (pageIndex, searchQuery) => {
  return createAction({
    type: types.GET_EXPORT_DATA_BY_PAGE_INDEX,
    callAPI: () => exportApi.getPasts(pageIndex, searchQuery)
    //shouldCallAPI: state => !state.pastExports.data.byPage[pageIndex]
  });
};

export const getPastExportsBySearchQuery = (pageIndex, searchQuery) => {
  return createAction({
    type: types.GET_EXPORT_DATA_BY_SEARCH_QUERY,
    callAPI: () => exportApi.getPasts(pageIndex, searchQuery)
    //shouldCallAPI: state => !state.pastExports.data.byPage[pageIndex]
  });
};

export const changePageIndex = pageIndex => {
  return { type: types.GET_EXPORT_DATA_CHANGE_PAGE_INDEX, pageIndex };
};

export const startExport = params => (dispatch, getState) => {
  return dispatch(
    createAction({
      type: types.START_EXPORT,
      callAPI: () => exportApi.start(params)
    })
  ).then(({ response, error }) => {
    if (error) return Promise.reject(error);
    if (response) {
      const { pastExports } = getState();
      dispatch({ type: types.ADD_EXPORT_DATA, data: response });
      dispatch(
        getPastExportsBySearchQuery(
          pastExports.data.pageIndex,
          pastExports.data.searchQuery
        )
      );
      return response;
    }
  });
};

export const retryExport = id => dispatch => {
  return exportApi.retry(id).then(data => {
    dispatch(updateExport(data));
  });
};

export const cancelExport = id => dispatch => {
  return exportApi.cancel(id).then(data => {
    dispatch(updateExport(data));
  });
};

export const updateExport = data => {
  return { type: types.CHANGE_EXPORT_DATA, data };
};
