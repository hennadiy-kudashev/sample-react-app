import CardApi from 'api/CardApi';
import { fromTo } from 'lib/reduxAsync';

export const getCard = () => {
  return fromTo(() => new CardApi().getCard(), ['card']);
};

export const updateCard = token => {
  return fromTo(() => new CardApi().updateCard(token), ['card']);
};
