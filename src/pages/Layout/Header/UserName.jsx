import { withCurrentUser } from 'components/Connectors';

const UserName = ({ currentUser }) => {
  return currentUser.name;
};

export default withCurrentUser(UserName);
