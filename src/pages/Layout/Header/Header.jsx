import React from 'react';
import logo from 'images/logo.png';
import { NavLink, Link } from 'react-router-dom';
import Collapse from 'reactstrap/lib/Collapse';
import { DropdownMenu } from 'components/Menu';
import Nav from 'reactstrap/lib/Nav';
import Navbar from 'reactstrap/lib/Navbar';
import UserName from './UserName';
import './Header.css';
import { Button } from 'components/Intercom';

class Header extends React.Component {
  state = {
    collapsed: true
  };

  handleToggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  render() {
    const { collapsed } = this.state;

    return (
      <Navbar color="white" light expand="md">
        <div className="container">
          <Link className="navbar-brand" to="/">
            <img src={logo} alt="..." />
          </Link>
          <button
            className={`navbar-toggler ${collapsed ? 'collapsed' : ''}`}
            type="button"
            aria-controls="navbarSupportedContent"
            aria-expanded={!collapsed}
            aria-label="Toggle navigation"
            onClick={this.handleToggle}
          >
            <span className="navbar-toggler-icon" />
          </button>
          <Collapse isOpen={!collapsed} navbar>
            <Nav className="ml-auto" navbar>
              <li className="nav-item">
                <NavLink className="nav-link" to="/exports">
                  Exports
                </NavLink>
              </li>
              <li className="nav-item d-none d-md-block">
                <span className="nav-link">
                  <span className="border border-left-0" />
                </span>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link text-danger" to="/upgrade">
                  Upgrade!
                </NavLink>
              </li>
              <DropdownMenu
                head={() => 'Support'}
                items={[
                  { tag: Button, label: 'Chat with us' },
                  { url: '/faq', label: 'FAQ' },
                  { url: '/api', label: 'API documentation' }
                ]}
              />
              <DropdownMenu
                head={UserName}
                items={[
                  { url: '/invoices-and-billing', label: 'Invoices & billing' },
                  { url: '/my-plan', label: 'My plan' },
                  { url: '/my-usage', label: 'My usage' },
                  { url: '/my-details', label: 'My details' },
                  {
                    url: 'https://myaccount.magimetrics.com/logout/',
                    label: 'Log out',
                    tag: 'a'
                  }
                ]}
              />
            </Nav>
          </Collapse>
        </div>
      </Navbar>
    );
  }
}

export default Header;
