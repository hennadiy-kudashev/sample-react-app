import React from 'react';
import { shallow } from 'enzyme';
import Layout from './Layout';

it('renders Header component', () => {
  const wrapper = shallow(<Layout />);
  expect(wrapper.find('Header').exists()).toEqual(true);
});
