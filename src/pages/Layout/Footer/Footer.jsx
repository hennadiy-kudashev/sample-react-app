import React from 'react';
import './Footer.css';

const Footer = () => (
  <footer className="footer">
    <div className="container">
      <div className="row">
        <div className="col-12 col-lg-6">
          <p className="text-center text-lg-left">
            © Copyright{' '}
            <span className="current-year">{new Date().getFullYear()}</span>{' '}
            Magi Metrics Ltd. All rights reserved.
          </p>
        </div>
        <div className="col-12 col-lg-6">
          <ul className="list-inline text-center text-lg-right">
            <li className="list-inline-item mr-3">
              <a href="https://www.magimetrics.com/about.html">Contact Us</a>
            </li>
            <li className="list-inline-item mr-3">
              <a href="https://www.magimetrics.com/cancellation-policy.html">
                Cancellation Policy
              </a>
            </li>
            <li className="list-inline-item">
              <a href="https://www.magimetrics.com/privacy-policy.html">
                Privacy Policy
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
);

export default Footer;
