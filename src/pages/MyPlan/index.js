import React from 'react';
import { Switch, Route } from 'react-router-dom';
import MyPlan from './MyPlan';
import CancelPage from './Cancel/CancelPage';
import Upgrade from './Upgrade';

const MyPlanPage = ({ match }) => (
  <Switch>
    <Route exact path={`${match.path}/`} component={MyPlan} />
    <Route path={`${match.path}/cancel`} component={CancelPage} />
    <Route path={`${match.path}/upgrade/:name`} component={Upgrade} />
  </Switch>
);

export default MyPlanPage;
