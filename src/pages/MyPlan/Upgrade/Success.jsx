import React from 'react';
import { Container } from 'components/Content';
import Confirmation from 'components/Confirmation';
import { withRouter } from 'react-router-dom';

const Success = ({ history }) => {
  return (
    <Container>
      <Confirmation
        type="info"
        header="Congratulations!"
        subheader="You’ve successfully subscribed to Magi Metrics."
        buttonText="Start your first export!"
        buttonProps={{
          onClick: () => history.push('/')
        }}
      >
        <Confirmation.Content>
          Welcome! If there is anything we can do to help, just let us know.
        </Confirmation.Content>
      </Confirmation>
    </Container>
  );
};

export default withRouter(Success);
