import React from 'react';
import { Section, Loading } from 'components/Content';
import ContainerWithPlan from '../ContainerWithPlan';
import STATUS from '../Plans/Item/status';
import plans from 'api/SubscriptionPlans';
import { Footer } from 'components/Stripe';
import { PaymentForm } from 'components/Quaderno';
import { withCurrentUser } from 'components/Connectors';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { subscribe } from 'actions/subscriptionActions';
import { getCard } from 'actions/cardActions';
import Success from './Success';

class Upgrade extends React.Component {
  state = {
    subscriptionSuccess: false
  };

  handleSubscription = params => {
    return this.props.subscribe(params).then(() => {
      this.setState({ subscriptionSuccess: true });
    });
  };

  componentWillMount() {
    if (!this.props.card.data) {
      this.props.getCard();
    }
  }

  renderPaymentForm(plan) {
    const { currentUser, card } = this.props;
    const initialValues = {
      firstName: currentUser.name,
      email: currentUser.email
    };
    if (card.loading) {
      return <Loading />;
    }
    return (
      <PaymentForm
        plan={plan}
        card={card.data && { ...card.data, name: currentUser.name }}
        initialValues={initialValues}
        onSubscription={this.handleSubscription}
      />
    );
  }

  render() {
    const { subscriptionSuccess } = this.state;

    if (subscriptionSuccess) {
      return <Success />;
    }
    const plan = plans.find(t => t.type === this.props.match.params.name);
    return (
      <ContainerWithPlan planType={plan.type} planStatus={STATUS.SELECTED}>
        <Section>
          <Section.Heading>
            Subscribe to {plan.label} for ${plan.price}/month
          </Section.Heading>
          <Section.Subheading>
            You can cancel for free at any time.
          </Section.Subheading>
          {this.renderPaymentForm(plan)}
        </Section>
        <Footer />
      </ContainerWithPlan>
    );
  }
}

const mapStateToProps = ({ card }) => ({ card });

export default compose(
  withCurrentUser,
  connect(mapStateToProps, { subscribe, getCard })
)(Upgrade);
