import React from 'react';
import Item from './Item';
import SubscriptionPlans from 'api/SubscriptionPlans';
import Status from './Item/status';
import { withCurrentUser } from 'components/Connectors';
import './Plans.css';

const getStatus = (type, current) => {
  if (type === current) {
    return Status.CURRENT;
  }
  if (
    SubscriptionPlans.findIndex(plan => plan.type === type) <
    SubscriptionPlans.findIndex(plan => plan.type === current)
  ) {
    return Status.DOWNGRADE;
  } else {
    return Status.UPGRADE;
  }
};

const Plans = ({ currentUser }) => {
  return (
    <div className="plan">
      <div className="row align-items-stretch no-gutters">
        {SubscriptionPlans.map(plan => (
          <div key={plan.type} className="col-12 col-lg-3">
            <Item
              type={plan.type}
              label={plan.label}
              price={plan.price}
              status={getStatus(plan.type, currentUser.subscriptionType)}
              features={plan.features}
              askForQuote={plan.askForQuote}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default withCurrentUser(Plans);
