const Status = {
  DOWNGRADE: 'downgrade',
  CURRENT: 'current plan',
  UPGRADE: 'upgrade',
  SELECTED: 'selected'
};

export default Status;
