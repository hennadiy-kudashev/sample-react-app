import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import { Button as IntercomButton } from 'components/Intercom';
import STATUS from './status';
import './Item.css';

const Item = ({
  type,
  label,
  features,
  price,
  askForQuote,
  status,
  active = false
}) => {
  const btnClasses = {
    'btn btn-lg btn-block': true,
    'btn-info': status === STATUS.DOWNGRADE,
    'btn-dark disabled': status === STATUS.CURRENT,
    'btn-primary': status === STATUS.UPGRADE,
    'btn-default disabled': status === STATUS.SELECTED
  };

  const renderButton = () => {
    if (askForQuote) {
      return (
        <IntercomButton className={cx(btnClasses)}>
          Ask for a quote
        </IntercomButton>
      );
    } else {
      return (
        <Link to={`/my-plan/upgrade/${type}`} className={cx(btnClasses)}>
          {status}
        </Link>
      );
    }
  };
  return (
    <div className={cx('plan-item', { active: active })}>
      <div className="plan-item-body">
        <h4 className="plan-item-body-heading">{label}</h4>
        <ul className="plan-item-body-features">
          {features.map((feature, i) => (
            <li key={`${label}_feature_${i}`}>{feature}</li>
          ))}
        </ul>
        <div className="plan-item-body-price">
          {price && [
            <small key="dollar">$</small>,
            ` ${price} `,
            <small key="month"> / month</small>
          ]}
        </div>
      </div>
      <div className="plan-item-footer">{renderButton()}</div>
    </div>
  );
};

Item.propTypes = {
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  features: PropTypes.array.isRequired,
  status: PropTypes.oneOf(Object.keys(STATUS).map(key => STATUS[key]))
    .isRequired,
  price: PropTypes.number,
  askForQuote: PropTypes.bool
};

export default Item;
