import React from 'react';
import { Link } from 'react-router-dom';
import { withCurrentUser } from 'components/Connectors';
import './Cancel.css';

const Cancel = ({ currentUser }) => {
  if (currentUser.isTrial) {
    return (
      <div className="row">
        <div className="col">
          <p className="plan-cancel" />
        </div>
      </div>
    );
  }
  return (
    <div className="row">
      <div className="col">
        <p className="plan-cancel">
          Looking to unsubscribe?{' '}
          <Link to="/my-plan/cancel" className="btn btn-primary">
            Cancel subscription
          </Link>
        </p>
      </div>
    </div>
  );
};

export default withCurrentUser(Cancel);
