import React from 'react';
import { Container, Section } from 'components/Content';
import ContainerWithPlan from '../ContainerWithPlan';
import STATUS from '../Plans/Item/status';
import { Button } from 'components/Form';
import { Link, withRouter } from 'react-router-dom';
import { withCurrentUser } from 'components/Connectors';
import { cancelSubscription } from 'actions/subscriptionActions';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Confirmation from 'components/Confirmation';
import Alert from 'components/Alert';

class CancelPage extends React.Component {
  state = {
    loading: false,
    error: '',
    unsubscriptionSuccess: false
  };

  handleClick = () => {
    this.setState({ loading: true });
    this.props
      .cancelSubscription()
      .then(() => {
        this.setState({
          loading: false,
          unsubscriptionSuccess: true
        });
      })
      .catch(({ message }) => {
        this.setState({
          loading: false,
          error: message
        });
      });
  };

  render() {
    const { currentUser, history } = this.props;
    const { loading, unsubscriptionSuccess, error } = this.state;

    if (unsubscriptionSuccess) {
      return (
        <Container>
          <Confirmation
            type="info"
            header="Done"
            subheader="You’ve successfully unsubscribed. "
            buttonText="Use trial exports"
            buttonProps={{
              onClick: () => history.push('/')
            }}
          />
        </Container>
      );
    }

    if (currentUser.isTrial) {
      return (
        <Container>
          <Section>
            <Section.Heading>You haven't subscribed yet.</Section.Heading>
            <Link to="/upgrade" className="btn btn-primary">
              Subscribe
            </Link>
          </Section>
        </Container>
      );
    }

    return (
      <ContainerWithPlan
        planType={currentUser.subscriptionType}
        planStatus={STATUS.CURRENT}
      >
        <Section>
          <Section.Heading>Are you sure you want to cancel?</Section.Heading>
          <Section.Subheading>
            Did something not work? Is a feature missing? Why not email us first
            at{' '}
            <a href="mailto:support@magimetrics.com">support@magimetrics.com</a>{' '}
            and we can help.
          </Section.Subheading>
          {error && <Alert type="error">{error}</Alert>}
          <div className="billing-form-submit">
            <Button
              type="submit"
              loading={loading}
              onClick={this.handleClick}
              className="btn btn-primary"
            >
              Unsubscribe
            </Button>
            <Link to="/my-plan" className="btn btn-light">
              Cancel
            </Link>
          </div>
        </Section>
      </ContainerWithPlan>
    );
  }
}

export default compose(
  withRouter,
  connect(null, { cancelSubscription }),
  withCurrentUser
)(CancelPage);
