import React from 'react';
import { Section } from 'components/Content';
import { Accordion, Card } from 'components/Accordion';
import pricingFaqs from 'api/PricingFaqs';

const Faq = () => {
  return (
    <div className="row">
      <div className="col">
        <Section className="faq">
          <Section.Heading>Questions about pricing:</Section.Heading>
          <Accordion>
            {pricingFaqs.map((item, i) => (
              <Card key={`pricing_faq_item_${i}`} title={item.title}>
                {item.content}
              </Card>
            ))}
          </Accordion>
        </Section>
      </div>
    </div>
  );
};

export default Faq;
