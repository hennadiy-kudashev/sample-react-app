import React from 'react';
import { ContainerWithRightBlock } from 'components/Content';
import PlanItem from '../Plans/Item';
import plans from '../../../api/SubscriptionPlans';

const ContainerWithPlan = ({ planType, planStatus, children }) => {
  const plan = plans.find(t => t.type === planType);
  return (
    <ContainerWithRightBlock
      rightBlock={() => <PlanItem {...plan} status={planStatus} active />}
    >
      {children}
    </ContainerWithRightBlock>
  );
};

export default ContainerWithPlan;
