import React from 'react';
import { Container } from 'components/Content';
import Plans from './Plans';
import Cancel from './Cancel';
import Faq from './Faq';

const MyPlan = () => {
  return (
    <Container title="My Plan">
      <Plans />
      <Cancel />
      <Faq />
    </Container>
  );
};

export default MyPlan;
