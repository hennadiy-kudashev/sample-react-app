import React from 'react';
import Layout from './Layout/Layout';
import { Switch, Route, Redirect } from 'react-router-dom';
import Faq from './Faq';
import Exports from './Exports';
import Invoices from './Invoices';
import MyPlan from './MyPlan';
import MyUsage from './MyUsage';
import MyDetails from './MyDetails';
import Api from './Api';
import { Chat } from 'components/Intercom';

const App = () => (
  <Layout>
    <Chat />
    <Switch>
      <Route exact path="/" render={() => <Redirect to="/exports" />} />
      <Route path="/faq" component={Faq} />
      <Route path="/exports" component={Exports} />
      <Route path="/invoices-and-billing" component={Invoices} />
      <Route path="/my-plan" component={MyPlan} />
      <Route path="/upgrade" render={() => <Redirect to="/my-plan" />} />
      <Route path="/my-usage" component={MyUsage} />
      <Route path="/my-details" component={MyDetails} />
      <Route path="/api" component={Api} />
    </Switch>
  </Layout>
);

export default App;
