import React from 'react';
import { Container, Section } from 'components/Content';
import Usage from './Usage';

const MyUsage = () => (
  <Container title="My Usage">
    <Section className="usage">
      <Usage />
    </Section>
  </Container>
);

export default MyUsage;
