import React from 'react';
import { Time } from 'components/Format';
import { Loading } from 'components/Content';
import Alert from 'components/Alert';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getUsageStatistics } from 'actions/usageActions';
import './Usage.css';

class Usage extends React.Component {
  componentWillMount() {
    this.props.getUsageStatistics();
  }

  render() {
    const { error, loading, data } = this.props;
    if (error) {
      return <Alert type="error">{error.message}</Alert>;
    }
    if (!data || loading) {
      return <Loading />;
    }

    const { actualCount, totalCount, resetDate } = data;
    const percent = actualCount / totalCount * 100;
    return (
      <div>
        <p className="usage-content">
          You have done <span>{actualCount}</span> exports out of{' '}
          <span>{totalCount}</span> exports this month.
        </p>
        <div className="usage-progress progress">
          <div
            className="progress-bar"
            role="progressbar"
            style={{ width: `${percent}%` }}
            aria-valuenow={percent}
            aria-valuemin="0"
            aria-valuemax="100"
          />
        </div>

        <p className="mb-4">
          Your usage will reset on <Time ms={resetDate} format="date" />.
        </p>

        <Link to="/upgrade" className="btn btn-primary">
          Upgrade
        </Link>
      </div>
    );
  }
}

const mapStateToProps = ({ usage }) => usage;

export default connect(mapStateToProps, { getUsageStatistics })(Usage);
