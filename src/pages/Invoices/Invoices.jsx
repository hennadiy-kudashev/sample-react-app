import React from 'react';
import { Section } from 'components/Content';
import { BillingButton } from 'components/Quaderno';

const Invoices = () => {
  return (
    <Section>
      <Section.Heading>Invoices & billing</Section.Heading>
      <BillingButton customerID="cus_CBP8ZKmlRabWCs" />
    </Section>
  );
};

export default Invoices;
