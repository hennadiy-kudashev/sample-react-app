import React from 'react';
import { Time } from 'components/Format';

const Card = ({ data: { name, last4, exp_month, exp_year, created } }) => (
  <div className="cards-item">
    <h6 className="cards-item-heading">
      {name} <span>xxxx</span>-{last4}
    </h6>
    <p className="cards-item-content">
      <span>
        {('0' + exp_month).slice(-2)}/{exp_year}
      </span>{' '}
      - Added on <Time ms={created * 1000} format="date" />
    </p>
  </div>
);

export default Card;
