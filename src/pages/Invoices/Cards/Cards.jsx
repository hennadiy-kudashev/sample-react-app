import React from 'react';
import { Section } from 'components/Content';
import { Switch, Route, withRouter } from 'react-router-dom';
import ViewCard from './ViewCard';
import EditCard from './EditCard';
import './Cards.css';

const Cards = ({ match }) => {
  return (
    <Section className="cards">
      <Switch>
        <Route exact path={`${match.path}`} component={ViewCard} />
        <Route path={`${match.path}/card`} component={EditCard} />
      </Switch>
    </Section>
  );
};

export default withRouter(Cards);
