import React from 'react';
import { Section } from 'components/Content';
import { Link } from 'react-router-dom';
import Card from './Card';
import { withCurrentUser } from 'components/Connectors';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { getCard } from 'actions/cardActions';
import { Loading } from 'components/Content';
import Alert from 'components/Alert';

class ViewCard extends React.Component {
  componentWillMount() {
    if (!this.props.data) {
      this.props.getCard();
    }
  }

  render() {
    const { loading, data, error, currentUser } = this.props;
    const cardExist = data && Object.keys(data).length > 0;
    return (
      <div>
        <div className="row">
          <div className="col">
            <Section.Heading>Credit / debit card</Section.Heading>
            {!cardExist && (
              <Link to="/invoices-and-billing/card" className="btn btn-primary">
                Add credit card
              </Link>
            )}
          </div>
          {cardExist && (
            <div className="col">
              <p className="cards-btn">
                <Link
                  to="/invoices-and-billing/card"
                  className="btn btn-primary"
                >
                  Change credit card
                </Link>
              </p>
            </div>
          )}
        </div>
        {error && (
          <Alert type="error">Failed to receive card: {error.message}</Alert>
        )}
        {loading && <Loading />}
        {cardExist && <Card data={{ ...data, name: currentUser.name }} />}
      </div>
    );
  }
}

const mapStateToProps = ({ card }) => card;

export default compose(withCurrentUser, connect(mapStateToProps, { getCard }))(
  ViewCard
);
