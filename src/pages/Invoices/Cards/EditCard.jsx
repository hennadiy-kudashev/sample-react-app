import React from 'react';
import { Section } from 'components/Content';
import { CardForm } from 'components/Stripe';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { updateCard } from 'actions/cardActions';

const EditCard = ({ updateCard, history }) => {
  const handleTokenReceived = token => {
    return updateCard(token).then(({ error }) => {
      if (error) {
        throw error;
      }
      history.push('/invoices-and-billing');
    });
  };
  return (
    <div>
      <div className="row">
        <div className="col">
          <Section.Heading>Update card</Section.Heading>
        </div>
      </div>
      <CardForm onTokenReceived={handleTokenReceived} />
    </div>
  );
};

export default compose(withRouter, connect(null, { updateCard }))(EditCard);
