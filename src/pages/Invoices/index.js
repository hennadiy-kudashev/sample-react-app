import React from 'react';
import { Container } from 'components/Content';
import Invoices from './Invoices';
import Cards from './Cards';

const InvoicesAndBillingPage = () => (
  <Container title="Invoices & Billing">
    <Invoices />
    <Cards />
  </Container>
);

export default InvoicesAndBillingPage;
