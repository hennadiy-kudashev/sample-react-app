import React from 'react';
import { withRouter } from 'react-router-dom';
import Confirmation from 'components/Confirmation';

const Fail = ({ history, location: { state: { error } } }) => {
  return (
    <Confirmation
      type="error"
      header="Export Failed"
      subheader={error.message}
      buttonText="OK"
      buttonProps={{
        onClick: () => history.push('/exports')
      }}
    />
  );
};

export default withRouter(Fail);
