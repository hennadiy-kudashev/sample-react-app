import React from 'react';
import Confirmation from 'components/Confirmation';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withCurrentUser } from 'components/Connectors';
import { withRouter, Redirect } from 'react-router-dom';
import { parse } from 'querystring';

const Started = ({ data, currentUser, history }) => {
  if (!data) {
    return <Redirect to="/exports" />;
  }
  const { description, time_estimation, followersToExport } = data;

  const getTimeEstimation = time => {
    function* text({ hours, minutes }) {
      if (hours > 0) {
        yield `${hours} hours`;
      }
      if (minutes > 0) {
        yield `${minutes} minutes`;
      }
    }
    return [...text(time)].join(' ');
  };

  return (
    <Confirmation
      type="info"
      header="Export Started"
      subheader={description}
      buttonText="OK"
      buttonProps={{
        onClick: () => {
          history.push('/exports');
        }
      }}
    >
      <Confirmation.Content>
        We estimate it’s going to take {getTimeEstimation(time_estimation)} to
        export your data.{' '}
        {followersToExport &&
          `Approximately 40% of Instagram accounts are private,
          so we estimate we will be able to export around ${
            followersToExport.estimate
          } of the ${followersToExport.total}
          followers.`}
      </Confirmation.Content>
      <Confirmation.Content>
        We will send you an email to{' '}
        <a href={`mailto:${currentUser.email}`}>{currentUser.email}</a> once
        it’s ready for you.
      </Confirmation.Content>
    </Confirmation>
  );
};

const mapStateToProps = ({ pastExports }, { location }) => {
  const id = parse(location.search.slice(1)).exportStarted;
  if (id) {
    return {
      data: pastExports.data.byId[id]
    };
  }
};

export default compose(withRouter, withCurrentUser, connect(mapStateToProps))(
  Started
);
