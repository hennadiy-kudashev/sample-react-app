import React from 'react';
import { Container } from 'components/Content';
import Search from './Search/Search';
import PastExports from './PastExports';

const ExportsPage = () => (
  <Container>
    <Search />
    <PastExports />
  </Container>
);

export default ExportsPage;
