import React from 'react';
import { Radio, Button } from 'components/Form';
import SearchType from 'api/SearchType';
import withSearchState from './withSearchState';

class Criteria extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonDisabled: true,
      ...props.getSearchState()
    };
  }

  componentWillReceiveProps(props) {
    if (props.location !== this.props.location) {
      this.setState(this.props.getSearchState(props.location));
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    e.stopPropagation();
    const query = {
      search_type: this.state.search_type,
      query: this.state.query
    };
    this.props.pushSearchState(query);
    this.setState({
      buttonDisabled: true
    });
  };

  handleRadioChangeFor = id => () => {
    this.setState({
      search_type: id,
      //enable button only is query is not empty.
      buttonDisabled: this.state.query === ''
    });
  };

  handleTextChange = e => {
    const query = e.target.value;
    this.setState({
      query,
      buttonDisabled: query === ''
    });
  };

  getRadioCheckedFor = id => {
    return this.state.search_type === id;
  };

  render() {
    const { query, buttonDisabled } = this.state;
    const { loading } = this.props;
    return (
      <form className="search-form" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label>Search for:</label>
          <Radio
            name="search-filter"
            label="an Instagram account"
            checked={this.getRadioCheckedFor(SearchType.ACCOUNT)}
            onChange={this.handleRadioChangeFor(SearchType.ACCOUNT)}
          />
          <Radio
            name="search-filter"
            label="an Instagram hashtag"
            checked={this.getRadioCheckedFor(SearchType.HASHTAG)}
            onChange={this.handleRadioChangeFor(SearchType.HASHTAG)}
          />
          <Radio
            name="search-filter"
            label="a geographic location"
            checked={this.getRadioCheckedFor(SearchType.LOCATION)}
            onChange={this.handleRadioChangeFor(SearchType.LOCATION)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="search" className="sr-only">
            Search here...
          </label>
          <input
            type="search"
            className="form-control form-control-lg"
            placeholder="Search here..."
            onChange={this.handleTextChange}
            value={query}
          />
          <Button
            type="submit"
            className="btn btn-lg btn-primary"
            loading={loading}
            disabled={buttonDisabled}
          >
            <i className="ion-ios-search-strong" />
          </Button>
        </div>
      </form>
    );
  }
}

export default withSearchState(Criteria);
