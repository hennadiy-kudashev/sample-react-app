import React from 'react';
import PropTypes from 'prop-types';
import { ClientListView } from 'components/ListView';
import ResultItem from './Item';
import Trial from './Trial';
import Back from './Back';
import Alert from 'components/Alert';
import { SEARCH_LIST_PAGE_SIZE } from 'const';
import './Results.css';

const Results = ({ data }) => {
  if (!data) {
    //initial state
    return null;
  }
  if (data.length === 0) {
    return (
      <Alert type="info">
        There were no search results for this search term.
      </Alert>
    );
  }
  return [
    <Back key="back" />,
    <Trial key="trial" />,
    <ClientListView
      key="list"
      getKeyFn={item => {
        const obj = item[Object.keys(item)[0]];
        return obj.instagram_id || obj.hashtag;
      }}
      pageSize={SEARCH_LIST_PAGE_SIZE}
      data={data}
      ItemComponent={ResultItem}
      listClassName="row"
    />
  ];
};

Results.propTypes = {
  data: PropTypes.array
};

export default Results;
