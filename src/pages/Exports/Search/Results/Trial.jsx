import React from 'react';
import { Link } from 'react-router-dom';
import { withCurrentUser } from 'components/Connectors';
import Alert from 'components/Alert';

const Trial = ({ currentUser }) => {
  if (!currentUser.isTrial) return null;
  return (
    <Alert type="info">
      Exports are limited to 100 rows during the free trial.{' '}
      <Link to="/upgrade">Upgrade to export more</Link>.
    </Alert>
  );
};

export default withCurrentUser(Trial);
