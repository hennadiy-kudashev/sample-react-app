import React from 'react';
import { withRouter } from 'react-router-dom';

const Back = props => {
  return (
    <div className="row">
      <div className="col">
        <div className="results-back">
          <button onClick={() => props.history.goBack()}>
            <i className="ion-ios-arrow-thin-left" />Back
          </button>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Back);
