import React from 'react';
import PropTypes from 'prop-types';
import { Radio } from 'components/Form';
import Hint from 'components/Hint';
import Collapse from 'reactstrap/lib/Collapse';

class Filter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      excludePrivate: true,
      followersMin: 0,
      followersMax: 999999,
      likesMin: 0,
      likesMax: 999999,
      collapsed: true
    };
    //in order to set initial state for Item component
    this.triggerOnChange();
  }

  handleToggleCollapse = () => {
    this.setState(({ collapsed }) => ({ collapsed: !collapsed }));
  };

  handleCheckboxChange = e => {
    this.setState(
      {
        excludePrivate: e.target.checked
      },
      this.triggerOnChange
    );
  };

  handleTextChangeFor = name => e => {
    this.setState(
      {
        [name]: parseInt(e.target.value, 10)
      },
      this.triggerOnChange
    );
  };

  triggerOnChange = () => {
    this.props.onChange({
      excludePrivate: this.state.excludePrivate,
      followersMin: this.state.followersMin,
      followersMax: this.state.followersMax,
      likesMin: this.state.likesMin,
      likesMax: this.state.likesMax
    });
  };

  render() {
    const {
      excludePrivate,
      followersMin,
      followersMax,
      likesMin,
      likesMax,
      collapsed
    } = this.state;
    return (
      <form className="results-item-filters">
        <div className="results-item-filters-header">
          <div className="form-group">
            <Radio
              type="checkbox"
              checked={excludePrivate}
              onChange={this.handleCheckboxChange}
            >
              Exclude private accounts from export{' '}
              <Hint>
                Approximately 40% of Instagram users choose to make their
                account private. For private accounts we can only provide the
                following fields: Instagram ID, Username, Full Name, Profile URL
                and Profile Picture URL. Ticking this box will skip those
                accounts.
              </Hint>
            </Radio>
          </div>
          <div className="form-group">
            <div className="dropdown">
              <button
                type="button"
                onClick={this.handleToggleCollapse}
                className="btn btn-link"
              >
                <i className="ion-ios-gear mr-1" />Advanced options
              </button>
            </div>
          </div>
        </div>
        <Collapse isOpen={!collapsed} className="results-item-filters-body">
          <p>
            Restrict the export to Instagram accounts, where their number of
            followers is:
          </p>
          <div className="results-item-filters-body-groups">
            <div className="form-group">
              <label>More than or equal to:</label>
              <input
                type="number"
                className="form-control form-control-sm"
                value={followersMin}
                onChange={this.handleTextChangeFor('followersMin')}
              />
            </div>
            <div className="form-group">
              <label>And less than or equal to:</label>
              <input
                type="number"
                className="form-control form-control-sm"
                value={followersMax}
                onChange={this.handleTextChangeFor('followersMax')}
              />
            </div>
          </div>
          <p>
            And who have an average number of likes per post that is between:
          </p>
          <div className="results-item-filters-body-groups">
            <div className="form-group">
              <label>More than or equal to:</label>
              <input
                type="number"
                className="form-control form-control-sm"
                value={likesMin}
                onChange={this.handleTextChangeFor('likesMin')}
              />
            </div>
            <div className="form-group">
              <label>And less than or equal to:</label>
              <input
                type="number"
                className="form-control form-control-sm"
                value={likesMax}
                onChange={this.handleTextChangeFor('likesMax')}
              />
            </div>
          </div>
        </Collapse>
      </form>
    );
  }
}

Filter.propTypes = {
  onChange: PropTypes.func.isRequired
};

export default Filter;
