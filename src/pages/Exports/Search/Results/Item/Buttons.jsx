import React from 'react';
import PropTypes from 'prop-types';
import SearchType from 'api/SearchType';
import ExportType from 'api/ExportType';
import { Button } from 'components/Form';
import { startExport } from 'actions/exportsActions';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

const types = {
  [SearchType.ACCOUNT]: ({ username }) => [
    {
      action: ExportType.FOLLOWED_BY,
      label: `Export accounts who follow @${username}`
    },
    {
      action: ExportType.FOLLOWS,
      label: `Export accounts @${username} follows`
    },
    {
      action: ExportType.POSTS,
      label: `Export posts by @${username}`
    },
    {
      action: ExportType.LIKES,
      label: `Export who likes @${username}'s posts`
    },
    {
      action: ExportType.COMMENTS,
      label: `Export who commented on @${username}'s posts`
    }
  ],
  [SearchType.HASHTAG]: ({ hashtag }) => [
    {
      action: ExportType.POSTS,
      label: `Export posts using #${hashtag}`
    },
    {
      action: ExportType.COMMENTS,
      label: `Export who commented on posts using #${hashtag}`
    }
  ],
  [SearchType.LOCATION]: () => [
    {
      action: ExportType.POSTS,
      label: `Export posts`
    }
  ]
};

class ResultItemButtons extends React.Component {
  state = {
    loadingAction: ''
  };

  handleClickFor = params => () => {
    this.setState({
      loadingAction: params.action
    });
    return this.props
      .startExport({
        ...params,
        filter: this.props.filter
      })
      .then(
        data =>
          this.props.history.push(`/exports?exportStarted=${data.export_id}`),
        error => this.props.history.push('/exports?exportFailed', { error })
      );
  };

  render() {
    const { item, actionDispatching } = this.props;
    const { loadingAction } = this.state;

    const type = Object.keys(item)[0];
    const searchItem = item[type];
    const buttons = types[type](searchItem);

    return (
      <ul className="results-item-body">
        {buttons.map(item => (
          <li key={item.action}>
            <Button
              loading={item.action === loadingAction}
              disabled={actionDispatching}
              className="btn btn-block btn-default"
              onClick={this.handleClickFor({
                type,
                action: item.action,
                ...searchItem
              })}
            >
              {item.label}
            </Button>
          </li>
        ))}
      </ul>
    );
  }
}

ResultItemButtons.propTypes = {
  item: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired
};
const mapStateToProps = ({ pastExports: { data: { actionDispatching } } }) => ({
  actionDispatching
});

export default compose(withRouter, connect(mapStateToProps, { startExport }))(
  ResultItemButtons
);
