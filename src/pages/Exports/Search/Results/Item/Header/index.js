import React from 'react';
import Account from './Account';
import Hashtag from './Hashtag';
import Location from './Location';
import SearchType from 'api/SearchType';

const types = {
  [SearchType.ACCOUNT]: Account,
  [SearchType.HASHTAG]: Hashtag,
  [SearchType.LOCATION]: Location
};

export default ({ item }) => {
  const type = Object.keys(item)[0];
  const Header = types[type];

  return <Header {...item[type]} />;
};
