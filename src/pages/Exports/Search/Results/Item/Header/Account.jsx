import React from 'react';

const Account = ({
  username,
  name,
  thumbnail,
  posts,
  followers,
  following
}) => (
  <div className="results-item-header">
    <div className="results-item-header-img">
      <img src={thumbnail} alt="..." />
    </div>
    <div>
      <h6 className="results-item-header-heading">
        {name} (@{username})
      </h6>
      <ul className="results-item-header-info">
        <li>
          <strong>{posts.toLocaleString()}</strong> posts
        </li>
        <li>
          <strong>{followers.toLocaleString()}</strong> followers
        </li>
        <li>
          <strong>{following.toLocaleString()}</strong> following
        </li>
      </ul>
    </div>
  </div>
);

export default Account;
