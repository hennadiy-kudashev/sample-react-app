import React from 'react';
import hashtagImg from 'images/hashtag-default.png';

const Hashtag = ({ hashtag, posts }) => (
  <div className="results-item-header">
    <div className="results-item-header-img">
      <img src={hashtagImg} alt="..." />
    </div>
    <div>
      <h6 className="results-item-header-heading">#{hashtag}</h6>
      <ul className="results-item-header-info">
        <li>
          <strong>{posts.toLocaleString()}</strong> posts
        </li>
      </ul>
    </div>
  </div>
);

export default Hashtag;
