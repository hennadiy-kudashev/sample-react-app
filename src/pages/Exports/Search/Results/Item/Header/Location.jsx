import React from 'react';

const Location = ({ location, thumbnail }) => (
  <div className="results-item-header">
    <div className="results-item-header-img">
      <img src={thumbnail} alt="..." />
    </div>
    <div>
      <h6 className="results-item-header-heading">{location}</h6>
    </div>
  </div>
);

export default Location;
