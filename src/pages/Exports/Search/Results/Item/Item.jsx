import React from 'react';
import Buttons from './Buttons';
import Filter from './Filter';
import Header from './Header';

class Item extends React.Component {
  state = {
    filter: {}
  };

  handleFilterChange = filter => {
    this.setState({ filter });
  };

  render() {
    const { item } = this.props;
    const { filter } = this.state;
    return (
      <div className="col-12 col-lg-6">
        <div className="results-item">
          <Header item={item} />
          <Filter onChange={this.handleFilterChange} />
          <Buttons item={item} filter={filter} />
        </div>
      </div>
    );
  }
}

export default Item;
