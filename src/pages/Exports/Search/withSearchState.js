import React from 'react';
import { stringify, parse } from 'querystring';
import SearchType from 'api/SearchType';
import { withRouter } from 'react-router-dom';
import isEqual from 'lodash/isEqual';

export default Composed => {
  return withRouter(props => {
    const extend = {
      pushSearchState(state) {
        props.history.push({
          pathname: props.location.pathname,
          search: stringify(state)
        });
      },
      getSearchState(location = props.location) {
        const { search_type = SearchType.ACCOUNT, query = '' } = parse(
          location.search.slice(1)
        );
        return { search_type, query };
      },
      isStateChanged(prevLocation, nextLocation) {
        return !isEqual(
          this.getSearchState(prevLocation),
          this.getSearchState(nextLocation)
        );
      }
    };

    return <Composed {...props} {...extend} />;
  });
};
