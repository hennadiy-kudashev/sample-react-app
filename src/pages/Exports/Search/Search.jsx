import React from 'react';
import Criteria from './Criteria';
import Results from './Results';
import { compose } from 'redux';
import { search, removeSearchData } from 'actions/searchActions';
import { withAction } from 'lib/reduxAsync';
import Alert from 'components/Alert';
import { Loading, Section } from 'components/Content';
import withSearchState from './withSearchState';
import {
  Fail as FailConfirmation,
  Started as StartedConfirmation
} from '../Confirmation';
import './Search.css';

class Search extends React.Component {
  doSearch = criteria => {
    if (criteria.query) {
      this.props.search(criteria);
    } else {
      this.props.removeSearchData();
    }
  };

  componentWillMount() {
    this.doSearch(this.props.getSearchState());
  }

  componentWillReceiveProps(props) {
    if (this.props.isStateChanged(this.props.location, props.location)) {
      this.doSearch(this.props.getSearchState(props.location));
    }
  }

  render() {
    const { loading, data, error, location } = this.props;
    if (location.search.includes('exportFailed')) {
      return <FailConfirmation />;
    }
    if (location.search.includes('exportStarted')) {
      return <StartedConfirmation />;
    }
    return (
      <Section className="search">
        <Section.Heading>Start a new export:</Section.Heading>
        <Criteria loading={loading} />
        {error && <Alert type="error">Search failed. {error.message}</Alert>}
        <Loading loading={!!loading}>
          <Results data={data} />
        </Loading>
      </Section>
    );
  }
}

export default compose(
  withSearchState,
  withAction({ search, removeSearchData })
)(Search);
