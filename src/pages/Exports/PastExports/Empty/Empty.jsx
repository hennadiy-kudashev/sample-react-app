import React from 'react';
import cricket from 'images/cricket.png';
import './Empty.css';

const Empty = () => (
  <div className="exports-empty">
    <h4 className="exports-header-heading">Past exports:</h4>
    <div className="exports-empty-icon">
      <img src={cricket} className="img-fluid" alt="..." />
    </div>
    <p className=" text-center text-muted mb-0">
      No previous exports found. <br />
      Please use the search form above to find the data you want to export.
    </p>
  </div>
);

export default Empty;
