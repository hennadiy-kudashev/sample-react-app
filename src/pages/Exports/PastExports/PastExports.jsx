import React from 'react';
import { connect } from 'react-redux';
import {
  getPastExportsByPageIndex,
  getPastExportsBySearchQuery,
  changePageIndex
} from 'actions/exportsActions';
import { Loading } from 'components/Content';
import { getDataByPageIndex } from 'reducers/pastExportsReducer';
import Alert from 'components/Alert';
import Header from './Header';
import Empty from './Empty';
import Items from './Items';

class PastExports extends React.Component {
  componentWillMount() {
    return this.props.getPastExportsBySearchQuery(0, '');
  }

  handlePageChange = pageIndex => {
    if (this.props.isDataAlreadyLoaded(pageIndex)) {
      this.props.changePageIndex(pageIndex);
    } else {
      this.props.getPastExportsByPageIndex(
        pageIndex,
        this.props.data.searchQuery
      );
    }
  };

  handleQueryChange = searchQuery => {
    this.props.getPastExportsBySearchQuery(0, searchQuery);
  };

  render() {
    const { data, loading, error } = this.props;

    if (error) {
      return [
        <Header key="header" onQueryChange={this.handleQueryChange} />,
        <Alert key="alert" type="error">
          Request failed. {error.message}
        </Alert>
      ];
    }
    if (!loading && data.items.length === 0 && data.searchQuery === '') {
      return <Empty />;
    }
    return [
      <Header
        key="header"
        onQueryChange={this.handleQueryChange}
        loading={loading}
      />,
      <Loading key="loading" loading={loading}>
        <Items data={data} onPageChange={this.handlePageChange} />
      </Loading>
    ];
  }
}

const mapStateToProps = ({ pastExports: { data, ...props } }) => {
  return {
    data: getDataByPageIndex(data),
    isDataAlreadyLoaded: pageIndex => !!data.byPage[pageIndex],
    ...props
  };
};

export default connect(mapStateToProps, {
  getPastExportsByPageIndex,
  getPastExportsBySearchQuery,
  changePageIndex
})(PastExports);
