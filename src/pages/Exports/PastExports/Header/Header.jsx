import React from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import './Header.css';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: ''
    };
    this.onQueryChange = debounce(this.props.onQueryChange, 1000);
  }

  handleChange = e => {
    const query = e.target.value;
    this.setState({ query }, () => {
      this.onQueryChange(query);
    });
  };

  render() {
    const { query } = this.state;
    const { loading } = this.props;

    return (
      <div className="exports-header">
        <div className="row align-items-center">
          <div className="col">
            <h4 className="exports-header-heading">Past exports:</h4>
          </div>
          <div className="col">
            <div className="exports-header-search">
              <div className="form-group">
                <label htmlFor="exports-search" className="sr-only">
                  Search in exports...
                </label>
                <input
                  type="search"
                  className="form-control"
                  placeholder="Search in exports..."
                  disabled={loading}
                  value={query}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  onChange: PropTypes.func
};

export default Header;
