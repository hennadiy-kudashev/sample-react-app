import React from 'react';
import Item from './Item';
import Alert from 'components/Alert';
import { ListView } from 'components/ListView';
import Limit from './Limit';
import { EXPORT_LIST_PAGE_SIZE } from 'const';

const Items = ({ data, onPageChange }) => {
  if (data.totalCount === '') return null;

  if (data.items.length === 0) {
    return (
      <Alert type="info">There were no exports for this search query.</Alert>
    );
  }

  return [
    <Limit key="limit" />,
    <ListView
      key="items"
      pageData={data.items}
      getKeyFn={item => item.export_id}
      pageIndex={data.pageIndex}
      totalCount={data.totalCount}
      ItemComponent={Item}
      pageSize={EXPORT_LIST_PAGE_SIZE}
      onPageChange={onPageChange}
    />
  ];
};

export default Items;
