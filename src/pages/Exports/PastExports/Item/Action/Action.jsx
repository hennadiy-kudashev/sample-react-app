import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'components/Form';
import cx from 'classnames';
import { retryExport, cancelExport } from 'actions/exportsActions';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import './Action.css';

class Action extends React.Component {
  state = {
    loading: false
  };

  handleClickFor = type => e => {
    //in order not to expand item panel
    e.stopPropagation();
    this.setState({ loading: true });
    const actionFn = this.props[`${type}Export`];
    actionFn(this.props.id).then(() => {
      this.hideLoading();
    });
  };

  hideLoading = () => {
    //in some cases component disappears after operation
    if (this._isMouned) {
      this.setState({ loading: false });
    }
  };

  componentDidMount() {
    this._isMouned = true;
  }
  componentWillUnmount() {
    this._isMouned = false;
  }

  render() {
    const { type } = this.props;
    const { loading } = this.state;

    const iconClassName = {
      'ion-ios-refresh-empty': type === 'retry',
      'ion-ios-close-empty': type === 'cancel'
    };

    return (
      <Button
        className="btn-image"
        loading={loading}
        title={type}
        onClick={this.handleClickFor(type)}
      >
        <i className={cx(iconClassName)} />
      </Button>
    );
  }
}

Action.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['cancel', 'retry']).isRequired
};

export default compose(
  withRouter,
  connect(null, { retryExport, cancelExport })
)(Action);
