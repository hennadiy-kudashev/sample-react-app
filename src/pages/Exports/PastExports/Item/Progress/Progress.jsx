import React from 'react';
import './Progress.css';

const Progress = ({ value, barClassName }) => {
  return [
    <div key="progress" className="progress">
      <div
        className={'progress-bar ' + barClassName}
        role="progressbar"
        style={{ width: `${value}%` }}
        aria-valuenow={value}
        aria-valuemin="0"
        aria-valuemax="100"
      />
    </div>,
    <div key="value" className="progress-value">
      {value}%
    </div>
  ];
};

export default Progress;
