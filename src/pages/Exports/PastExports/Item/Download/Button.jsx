import React from 'react';

const Button = ({ url, number }) => [
  <a key="button" href={url} className="btn btn-sm btn-primary">
    <i className="ion-document-text" /> Download CSV file {number}
  </a>,
  ' '
];

export default Button;
