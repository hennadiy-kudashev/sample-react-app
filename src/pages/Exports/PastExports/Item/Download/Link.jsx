import React from 'react';

const Link = ({ url, number }) => [
  <a key="link" href={url}>
    CSV {number}
  </a>,
  ', '
];

export default Link;
