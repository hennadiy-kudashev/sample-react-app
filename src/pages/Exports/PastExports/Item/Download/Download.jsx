import React from 'react';
import Button from './Button';
import Link from './Link';
import './Download.css';

const Download = ({ files }) => {
  if (files.length === 0) {
    return null;
  }

  const Item = files.length > 10 ? Link : Button;
  return (
    <div className="col">
      <ul className="exports-item-body-stats">
        <li>
          <span>Download:</span>
        </li>
        <li>{files.map((file, i) => <Item key={`file${i}`} {...file} />)}</li>
      </ul>
    </div>
  );
};

export default Download;
