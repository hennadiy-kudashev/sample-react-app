import React from 'react';
import { Time } from 'components/Format';
import { Duration } from 'components/Format';
import Collapse from 'reactstrap/lib/Collapse';
import Progress from './Progress';
import ExportStatus from 'api/ExportStatus';
import Download from './Download';
import Action from './Action/Action';
import './Item.css';

const statusClasses = {
  [ExportStatus.IN_QUEUE]: 'secondary',
  [ExportStatus.CANCELLED]: 'warning',
  [ExportStatus.IN_PROGRESS]: 'info',
  [ExportStatus.SUCCESS]: 'success',
  [ExportStatus.FAILED]: 'danger'
};

class Item extends React.Component {
  state = {
    collapsed: true
  };

  handleToggleCollapse = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  render() {
    const {
      export_id,
      status,
      description,
      thumbnail,
      progress,
      started_at,
      completed_at,
      csv_files
    } = this.props.item;
    const { collapsed } = this.state;

    const bgClass = `bg-${statusClasses[status]}`;
    const badgeClass = `badge-${statusClasses[status]}`;

    return (
      <div className="exports-item" role="tablist">
        <div
          className={`exports-item-header ${bgClass}`}
          onClick={this.handleToggleCollapse}
          aria-expanded={!collapsed}
          role="tab"
        >
          <div className="exports-item-header-img">
            <img src={thumbnail} alt="..." />
          </div>
          <div>
            <h6 className="exports-item-header-heading">{description}</h6>
            <ul className="exports-item-header-timing">
              <li>
                <i className="ion-ios-calendar-outline" />{' '}
                <Time ms={started_at} format="monthShort" />
              </li>
              <li>
                <i className="ion-ios-clock-outline" />{' '}
                <Duration start={started_at} plusAgo />
              </li>
              <li>
                <span className={`badge ${badgeClass}`}>{status}</span>
              </li>
            </ul>
          </div>
          <div className="exports-item-header-extra">
            {(status === ExportStatus.IN_PROGRESS ||
              status === ExportStatus.IN_QUEUE) && (
              <Action type="cancel" id={export_id} />
            )}
            {(status === ExportStatus.FAILED ||
              status === ExportStatus.CANCELLED) && (
              <Action type="retry" id={export_id} />
            )}
            <Progress value={progress} barClassName={bgClass} />
          </div>
        </div>
        <Collapse className="exports-item-body" isOpen={!collapsed}>
          <div className="exports-item-body-inner">
            <div className="row">
              <div className="col">
                <ul className="exports-item-body-stats">
                  <li>
                    <span>Started:</span>{' '}
                    <Time ms={started_at} format="dateTime" />
                  </li>
                  <li>
                    <span>Duration:</span>{' '}
                    <Duration
                      start={started_at}
                      end={completed_at}
                      plusAgo={false}
                    />
                  </li>
                  <li>
                    <span>Status:</span> {status}
                  </li>
                </ul>
              </div>
              {status === ExportStatus.SUCCESS && (
                <Download files={csv_files} />
              )}
            </div>
          </div>
        </Collapse>
      </div>
    );
  }
}

export default Item;
