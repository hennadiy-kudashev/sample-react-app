import React from 'react';
import { Section } from 'components/Content';
import PastExports from './PastExports';

export default () => (
  <Section className="exports">
    <PastExports />
  </Section>
);
