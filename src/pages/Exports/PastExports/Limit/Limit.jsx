import React from 'react';
import { Link } from 'react-router-dom';
import { LEFT_NUMBER_OF_EXPORTS_TO_SHOW_WARNING } from 'const';
import { getUsageStatistics } from 'actions/usageActions';
import { connect } from 'react-redux';
import './Limit.css';

class Limit extends React.Component {
  componentWillMount() {
    this.props.getUsageStatistics();
  }

  render() {
    const { data } = this.props;
    if (!data) {
      return null;
    }
    const { actualCount, totalCount } = data;
    const leftCount = totalCount - actualCount;
    if (leftCount > LEFT_NUMBER_OF_EXPORTS_TO_SHOW_WARNING) {
      return null;
    }
    return (
      <div className="exports-alert alert alert-primary">
        You can do {leftCount} more exports this month!{' '}
        <Link to="/my-usage">Show my usage!</Link>
      </div>
    );
  }
}

const mapStateToProps = ({ usage }) => usage;

export default connect(mapStateToProps, { getUsageStatistics })(Limit);
