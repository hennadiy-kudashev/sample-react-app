import React from 'react';
import './Item.css';

const Item = ({ title, id, children }) => {
  return (
    <div className="api-item" id={id}>
      <h5 className="api-heading">{title}</h5>
      {children}
    </div>
  );
};

export default Item;
