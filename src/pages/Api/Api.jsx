import React from 'react';
import { ContainerWithAside, Section } from 'components/Content';
import Item from './Item';
import apiInformation from 'api/ApiInformation';
import { withCurrentUser } from 'components/Connectors';

const Api = ({ currentUser }) => {
  const items = apiInformation(currentUser.apiKey);
  return (
    <ContainerWithAside
      title="API"
      menuItems={items.map((item, index) => ({
        url: `api-item-${index}`,
        label: item.title
      }))}
    >
      <Section className="api">
        <Section.Heading>API documentation</Section.Heading>
        <Section.Subheading>
          The Magi Metrics API makes it easy to run large numbers of exports and
          allows you to integrate Magi Metrics with your own system.
        </Section.Subheading>
        {items.map((item, index) => (
          <Item
            key={`api-item-${index}`}
            title={item.title}
            id={`api-item-${index}`}
          >
            {item.content}
          </Item>
        ))}
      </Section>
    </ContainerWithAside>
  );
};

export default withCurrentUser(Api);
