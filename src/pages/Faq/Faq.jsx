import React from 'react';
import { Accordion, Card } from 'components/Accordion';
import { Container, Section } from 'components/Content';
import faqs from 'api/Faqs';

const Faq = () => (
  <Container>
    <Section className="faq">
      <Section.Heading>Frequently asked questions</Section.Heading>
      <Section.Subheading>
        Didn’t find what you were looking for? Write us an email:{' '}
        <a href="mailto:support@magimetrics.com">support@magimetrics.com</a>
      </Section.Subheading>
      <Accordion>
        {faqs.map((faq, i) => (
          <Card key={`faq-item-${i}`} title={faq.title}>
            {faq.content}
          </Card>
        ))}
      </Accordion>
    </Section>
  </Container>
);

export default Faq;
