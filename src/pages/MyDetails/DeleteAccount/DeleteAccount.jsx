import React from 'react';
import { Section } from 'components/Content';
import { Link } from 'react-router-dom';

const DeleteAccount = () => {
  return (
    <Section>
      <Section.Heading>Extreme measures:</Section.Heading>
      <Link to="/my-details/delete-account">
        Delete your Magi Metrics Account
      </Link>
    </Section>
  );
};

export default DeleteAccount;
