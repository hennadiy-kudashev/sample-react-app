import React from 'react';
import { Section } from 'components/Content';
import Alert from 'components/Alert';
import { Button } from 'components/Form';
import { Link } from 'react-router-dom';
import { deleteAccount } from 'actions/userActions';
import { connect } from 'react-redux';

class DeleteAccountPage extends React.Component {
  state = {
    submitting: false,
    error: ''
  };

  handleClick = () => {
    this.setState({ submitting: true });
    this.props
      .deleteAccount()
      .then(() => {
        window.location.href =
          'https://www.magimetrics.com/account-deleted.html';
        this.setState({
          submitting: false
        });
      })
      .catch(({ message }) => {
        this.setState({
          submitting: false,
          error: message
        });
      });
  };

  render() {
    const { error, submitting } = this.state;

    return (
      <Section>
        <Section.Heading>
          Are you sure you want to delete account?
        </Section.Heading>
        <Section.Subheading>
          Did something not work? Is a feature missing? Why not email us first
          at{' '}
          <a href="mailto:support@magimetrics.com">support@magimetrics.com</a>{' '}
          and we can help.
        </Section.Subheading>
        {error && <Alert type="error">{error}</Alert>}
        <div className="billing-form-submit">
          <Button
            type="submit"
            loading={submitting}
            onClick={this.handleClick}
            className="btn btn-primary"
          >
            Delete Account
          </Button>
          <Link to="/my-details" className="btn btn-light">
            Cancel
          </Link>
        </div>
      </Section>
    );
  }
}

export default connect(undefined, { deleteAccount })(DeleteAccountPage);
