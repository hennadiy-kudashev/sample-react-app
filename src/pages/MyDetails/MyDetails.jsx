import React from 'react';
import { Container } from 'components/Content';
import Details from './Details';
import ChangePassword from './ChangePassword';
import DeleteAccount, { DeleteAccountPage } from './DeleteAccount';
import { Route, Switch } from 'react-router-dom';

const MyDetails = ({ match }) => {
  return (
    <Container title="My Details">
      <Switch>
        <Route
          exact
          path={`${match.path}/`}
          render={() => (
            <div>
              <Details />
              <ChangePassword />
              <DeleteAccount />
            </div>
          )}
        />
        <Route
          path={`${match.path}/delete-account`}
          component={DeleteAccountPage}
        />
      </Switch>
    </Container>
  );
};

export default MyDetails;
