import React from 'react';
import reduxForm from 'redux-form/es/reduxForm';
import { Button, FormGroupFieldWide } from 'components/Form';
import Alert from 'components/Alert';

const DetailsForm = ({
  submitting,
  pristine,
  handleSubmit,
  error,
  submitSucceeded,
  anyTouched
}) => {
  return (
    <form className="details-form" onSubmit={handleSubmit}>
      {submitSucceeded &&
        !anyTouched && (
          <Alert type="success">
            You've updated your First name successfully.
          </Alert>
        )}
      {error && <Alert type="error">{error}</Alert>}
      <FormGroupFieldWide name="firstName" label="First name:" required />
      <div className="form-group">
        <div className="row">
          <div className="col offset-md-3 offset-lg-2">
            <Button
              className="btn btn-primary"
              disabled={pristine}
              loading={submitting}
            >
              Save
            </Button>
          </div>
        </div>
      </div>
    </form>
  );
};

export default reduxForm({
  form: 'details',
  //in order to hide success message on input change
  touchOnChange: true,
  //on order to reset touch props(to show success message)
  enableReinitialize: true
})(DetailsForm);
