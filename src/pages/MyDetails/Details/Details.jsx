import React from 'react';
import { Section } from 'components/Content';
import DetailsForm from './DetailsForm';
import { withCurrentUser } from 'components/Connectors';
import SubmissionError from 'redux-form/es/SubmissionError';
import { updateUser } from 'actions/userActions';
import { connect } from 'react-redux';
import { compose } from 'redux';
import './Details.css';

const Details = ({ updateUser, currentUser }) => {
  const handleSubmit = values => {
    return updateUser(values).catch(error => {
      throw new SubmissionError({ _error: error.message });
    });
  };

  return (
    <Section className="details">
      <Section.Heading>Your details:</Section.Heading>
      <DetailsForm
        onSubmit={handleSubmit}
        initialValues={{ firstName: currentUser.name }}
      />
    </Section>
  );
};

export default compose(withCurrentUser, connect(undefined, { updateUser }))(
  Details
);
