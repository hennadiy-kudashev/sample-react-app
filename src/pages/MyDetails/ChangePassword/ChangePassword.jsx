import React from 'react';
import { Section } from 'components/Content';
import ChangePasswordForm from './ChangePasswordForm';
import SubmissionError from 'redux-form/es/SubmissionError';
import { changePassword } from 'actions/userActions';
import { connect } from 'react-redux';

const ChangePassword = ({ changePassword }) => {
  const handleSubmit = values => {
    return changePassword(values).catch(error => {
      throw new SubmissionError({ _error: error.message });
    });
  };
  return (
    <Section>
      <Section.Heading>Change password:</Section.Heading>
      <ChangePasswordForm onSubmit={handleSubmit} />
    </Section>
  );
};

export default connect(undefined, { changePassword })(ChangePassword);
