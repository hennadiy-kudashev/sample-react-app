import React from 'react';
import reduxForm from 'redux-form/es/reduxForm';
import { Button, FormGroupFieldWide } from 'components/Form';
import Alert from 'components/Alert';

const validate = values => {
  const errors = {};
  if (values.password !== values.confirmPassword) {
    errors.confirmPassword = 'Passwords must match.';
  }
  return errors;
};

const ChangePasswordForm = ({
  submitting,
  pristine,
  handleSubmit,
  error,
  submitSucceeded,
  anyTouched
}) => {
  return (
    <form className="details-form" onSubmit={handleSubmit}>
      {submitSucceeded &&
        !anyTouched && (
          <Alert type="success">
            You've changed your password successfully.
          </Alert>
        )}
      {error && <Alert type="error">{error}</Alert>}
      <FormGroupFieldWide
        name="password"
        label="New password:"
        type="password"
        required
      />
      <FormGroupFieldWide
        name="confirmPassword"
        label="Confirm password:"
        type="password"
        required
      />
      <div className="form-group">
        <div className="row">
          <div className="col offset-md-3 offset-lg-2">
            <Button
              className="btn btn-primary"
              disabled={pristine}
              loading={submitting}
            >
              Save
            </Button>
          </div>
        </div>
      </div>
    </form>
  );
};

export default reduxForm({
  form: 'changePassword',
  validate,
  //in order to hide success message on input change
  touchOnChange: true
})(ChangePasswordForm);
