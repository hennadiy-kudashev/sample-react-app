# Sample React App

Demo - https://sample-reactjs-app.herokuapp.com/

## Development

#### Setup

Create `.env.local` file from `.env.local.example` and fill in the required
environment variables.
```
cp .env.local.example .env.local
```

##### ENV VARS

- `REACT_APP_STRIPE_PUBLISHABLE_KEY` - [Stripe](https://stripe.com) publishable key.
- `REACT_APP_QUADERNO_PUBLISHABLE_KEY` - [Quaderno](https://quaderno.io/) publishable key.

##### Start dev server

```
yarn install
yarn start
```

Dev server starts under http://localhost:3000.

##### Run unit tests

```
yarn test
```

## Production

>Note: Make sure that `.env.production` file has values defined which is used on production.

Next commands generates `build` directory:
```
yarn install
yarn build
```

## Docker image

First [production artifacts](#production) should be build.
Next command create local docker image:
```
yarn docker-image
```

>Note: In the compose-examle.yml you could see how to integrate the image.